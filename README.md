![Reseptisovellus ikoni](https://gitlab.com/eeronen/reseptisovellus-react/raw/master/favicon.png "Reseptisovellus")
# Reseptisovellus
Reseptisovellus, jonka avulla voidaan selata, lisätä ja muokata reseptejä.

## Kehitysympäristö
Backend on pakon edessä muutettu käyttämään mySQL:ää ja PHP:ta. Helpoin tapa saada homma rullaamaan on asentamalla [XAMPP](https://www.apachefriends.org/index.html). Esimerkkidata on tiedostossa reseptitietokanta.sql. Reseptisovelluksen `build`-kansio kannattaa sen jälkeen linkittää xamppin kansiosta. Kehitys käy helpoiten käyttämällä komentoa `npm run watch`
