let environment = {
    apiUrl: "http://localhost:8080/api",
};

function getRecipes() {
    return new Promise((resolve, reject) => {
        const xmlhttp = new XMLHttpRequest();
        xmlhttp.onload = () => {
            resolve(JSON.parse(xmlhttp.responseText));
        };
        xmlhttp.onerror = () => reject(xmlhttp.statusText);
        xmlhttp.open("GET", environment.apiUrl + "/deprecated/get_recipes.php", true);
        xmlhttp.send();
    });
}

function putRecipe(data) {
    return new Promise((resolve, reject) => {
        const xmlhttp = new XMLHttpRequest();
        xmlhttp.onload = () => {
            if (xmlhttp.status === 200) {
                resolve(true);
            } else {
                reject(false);
            }
        };
        xmlhttp.onerror = () => reject(xmlhttp.statusText);
        xmlhttp.open("POST", environment.apiUrl + "/post_recipe.php?is_new=true", true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.send(`json_data=${encodeURIComponent(JSON.stringify(data))}`);
    });
}

(async () => {
    const recipes = await getRecipes();

    recipes.forEach(async recipe => {
        console.log(`converting ${recipe.name}`);
        await putRecipe(recipe);
    });

    console.log("done");
})();
