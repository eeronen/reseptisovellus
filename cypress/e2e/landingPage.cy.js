import { BASE_URL } from "../support/e2e";

const logo = () => cy.get(".landing-page-title__logo");

describe("Landing page", () => {
    beforeEach(() => {
        cy.visit(BASE_URL);
    });
    it("Should display all items", () => {
        cy.visit(BASE_URL);
        cy.contains("Omat suosikit");
        cy.contains("Viimeksi katsotut");
    });

    it("Clicking the hat should take to all recipes", () => {
        logo().click();
        cy.url().should("include", "reseptit");
        cy.contains("Kategoriat");
        cy.contains("Kaikki");
    });
});
