import { BASE_URL } from "../support/e2e";

const searchIcon = () => cy.get(".search__icon");
const searchInput = () => cy.get(".search__input");

describe("Menu", () => {
    beforeEach(() => {
        cy.visit(BASE_URL);
    });
    it("Should display all items", () => {
        searchIcon().should("exist");
        cy.contains("Etusivu");
        cy.contains("Reseptit");
        cy.contains("Uusi resepti");
        cy.contains("Hallitse reseptejä");
    });

    it("Search should be expandable", () => {
        cy.visit(BASE_URL);
        searchIcon().click();
        searchInput().should("exist");
        cy.contains("Sulje").click();
        searchIcon().should("exist");
        searchInput().should("not.be.visible");
    });

    it("Should do search and display matching links", () => {
        cy.visit(BASE_URL);
        searchIcon().click();
        searchInput().type("1");
        cy.contains("recipe1").click();
        cy.contains("ingredient1");
    });

    it("Should navigate with menu", () => {
        cy.visit(BASE_URL);
        cy.contains("Reseptit").click();
        cy.contains("Kategoriat");
    });
});
