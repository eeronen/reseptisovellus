import { BASE_URL } from "../support/e2e";

const ingredientItemClass = ".ingredient-block";
const inputContainer = (title) => cy.get(".recipeform-block").contains(title);
const inputField = (title) => inputContainer(title).find(".recipeform__input");

const cookingTimeContainer = () => inputContainer("Valmistusaika");
const cookingTimeField = () => inputField("Valmistusaika");

const portionsContainer = () => inputContainer("Annokset");
const portionsField = () => inputField("Annokset");

const addIngredientButton = () => cy.contains("Lisää aines");
const addSubtitleButton = () => cy.contains("Lisää alaotsikko");

const shouldHaveXIngredients = (x) => cy.get(ingredientItemClass).should("have.lengthOf", x);
const ingredientBlock = (n) => cy.get(ingredientItemClass).eq(n);
const ingredientNameInput = (n) => ingredientBlock(n).find("input").eq(2);
const ingredientQuantityInput = (n) => ingredientBlock(n).find("input").eq(0);
const ingredientUnitInput = (n) => ingredientBlock(n).find("input").eq(1);
const removeNthIngredient = (n) => ingredientBlock(n).find(".icon-remove").click();
const subtitleInput = (n) => ingredientBlock(n).find("input").eq(0);

const getInstructionBlock = () => cy.get(".instruction-block");
const shoudContainNInstructions = (n) => getInstructionBlock().should("have.lengthOf", n);
const getNthInstruction = (n) => getInstructionBlock().eq(n);
const getInstructionInput = (n) => getNthInstruction(n).find("input");
const addInstruction = () => cy.contains("Lisää työvaihe").click();

const RECIPENAME = "testRecipe";

describe("Recipe editing", () => {
    beforeEach(() => {
        cy.visit(`${BASE_URL}#/reseptin-muokkaus/uusi`);
    });
    describe("Basic info", () => {
        it("Should display errors for empty name", () => {
            inputContainer("Reseptin nimi:").contains("Kenttää ei voi jättää tyhjäksi");
            inputField("Reseptin nimi:").type(RECIPENAME);
            inputContainer("Reseptin nimi:")
                .contains("Kenttää ei voi jättää tyhjäksi")
                .should("not.exist");
        });

        it("Category should be selectable", () => {
            inputField("Kategoria").select("Pääruuat");
        });

        it("Should not allow incorrect cooking times", () => {
            cookingTimeField().clear();
            cookingTimeContainer().contains("Kenttää ei voi jättää tyhjäksi");

            cookingTimeField().type("-1");
            cookingTimeContainer().contains("Arvo ei voi olla alle 0");

            cookingTimeField().clear().type("1");
            cookingTimeContainer().contains("ei voi").should("not.exist");
        });

        it("Should not allow incorrect portions", () => {
            portionsField().clear();
            portionsContainer().contains("Kenttää ei voi jättää tyhjäksi");

            portionsField().type("-1");
            portionsContainer().contains("Arvo ei voi olla alle 0");

            portionsField().clear().type("1");
            portionsContainer().contains("ei voi").should("not.exist");
        });
    });

    describe("Ingredients", () => {
        it("Should add subtitles", () => {
            shouldHaveXIngredients(0);
            addSubtitleButton().click();
            shouldHaveXIngredients(1);
            subtitleInput(0).type("subtitle");
        });

        it("Should add ingredient", () => {
            addIngredientButton().click();
            addIngredientButton().click();
            shouldHaveXIngredients(2);
            ingredientBlock(1);
            ingredientQuantityInput(1).type("1");
            ingredientUnitInput(1).type("ml");
            ingredientNameInput(1).type("ingredient1");
        });

        it("Should reorder ingredients", () => {
            addIngredientButton().click();
            addIngredientButton().click();
            ingredientNameInput(0).type("ingredient0");
            ingredientNameInput(1).type("ingredient1");

            shouldHaveXIngredients(2);
            ingredientNameInput(1).click();
            ingredientBlock(1).find(".moveup").click();
            ingredientNameInput(1).should("have.value", "ingredient0");

            ingredientBlock(0).find(".movedown").click();
            ingredientNameInput(1).should("have.value", "ingredient1");
        });

        it("Should remove ingredients", () => {
            addIngredientButton().click();
            addIngredientButton().click();
            addIngredientButton().click();

            shouldHaveXIngredients(3);
            ingredientNameInput(1).click();
            removeNthIngredient(1);
            shouldHaveXIngredients(2);
            subtitleInput(0).click();
            removeNthIngredient(0);
            shouldHaveXIngredients(1);
            removeNthIngredient(0);
            cy.get(ingredientItemClass).should("not.exist");
        });
    });

    describe("Instructions", () => {
        it("Should add instructions", () => {
            addInstruction();
            shoudContainNInstructions(1);
            getInstructionInput(0).type("instruction1");

            addInstruction();
            shoudContainNInstructions(2);
            getInstructionInput(1).type("instruction2");
        });

        it("Should move instructions", () => {
            addInstruction();
            addInstruction();
            getInstructionInput(0).type("instruction1");
            getInstructionInput(1).type("instruction2");

            getNthInstruction(1).click().find(".moveup").click();
            getInstructionInput(0).should("have.value", "instruction2");
            getInstructionInput(1).should("have.value", "instruction1");

            getNthInstruction(0).click().find(".movedown").click();
            getInstructionInput(1).should("have.value", "instruction2");
            getInstructionInput(0).should("have.value", "instruction1");
        });

        it("Should remove instructions", () => {
            addInstruction();
            addInstruction();

            shoudContainNInstructions(2);
            getNthInstruction(1).click().find(".icon-remove").click();
            shoudContainNInstructions(1);
            getNthInstruction(0).click().find(".icon-remove").click();
            getInstructionBlock().should("not.exist");
        });
    });

    describe("Add recipe", () => {
        before(() => {
            cy.visit(`${BASE_URL}#/reseptin-muokkaus/uusi`);

            inputField("Reseptin nimi:").type(RECIPENAME);
            inputField("Valmistusaika tunteina:").type(1);
            inputField("Annokset:").type(1);

            addIngredientButton().click();
            ingredientQuantityInput(0).clear().type("1");
            ingredientUnitInput(0).type("ml");
            ingredientNameInput(0).type("ingredient1");

            addInstruction();
            getInstructionInput(0).type("instruction1");
        });

        it.skip("Should react to server outage", () => {
            cy.get(".submitbutton.submit").click();
        });

        it("Should add new recipe", () => {
            cy.intercept("POST", "/api/post_recipe.php", "OK");

            cy.get(".submitbutton.submit").click();
            cy.contains("Resepti lisätty");
            cy.contains("Ok").click();
        });
    });

    describe("Edit Recipe", () => {
        beforeEach(() => {
            cy.visit(`${BASE_URL}#/hallitse`);
        });
        it("Should display error for wrong password", () => {
            cy.intercept("POST", "/api/check_token.php", {
                statusCode: 401,
                response: "",
            });
            cy.intercept("POST", "/api/login.php", {
                statusCode: 401,
                response: "",
            });
            cy.visit(`${BASE_URL}#/hallitse`);
            cy.contains("Anna salasana reseptien muokkaukseen");
            cy.get("input.password-input").type("wrongpassword");
            cy.contains("Kirjaudu").click();
            cy.contains("Virheellinen salasana");
        });

        it("Should open recipe for editing", () => {
            cy.intercept("POST", "/api/login.php", {
                statusCode: 200,
                response: "ok",
            });
            cy.intercept("POST", "/api/check_token.php*", { statusCode: 200, response: "valid" });
            cy.get("input.password-input").type("testpassword");
            cy.contains("Kirjaudu").click();
            cy.contains("recipe1").click();

            ingredientNameInput(0).should("have.value", "ingredient1");
            inputField("Reseptin nimi").should("have.value", "recipe1");

            cy.contains("Peru muutokset");
            cy.contains("Poista Resepti");
        });

        it("Should cancel edits", () => {
            cy.intercept("POST", "/api/login.php", {
                statusCode: 200,
                response: "ok",
            });
            cy.intercept("POST", "/api/check_token.php*", { statusCode: 200, response: "valid" });
            cy.get("input.password-input").type("testpassword");
            cy.contains("Kirjaudu").click();
            cy.contains("recipe1").click();
            addIngredientButton().click();
            addSubtitleButton().click();
            addInstruction();
            shoudContainNInstructions(3);
            shouldHaveXIngredients(3);

            cy.get(".submitbutton.submit").scrollIntoView();
            cy.contains("Peru muutokset").click();
            shoudContainNInstructions(2);
            shouldHaveXIngredients(1);
        });

        it("Should delete recipe", () => {
            cy.intercept("POST", "/api/login.php", {
                statusCode: 200,
                response: "ok",
            });
            cy.intercept("POST", "/api/check_token.php*", { statusCode: 200, response: "valid" });

            cy.intercept("DELETE", "/api/delete_recipe.php*", { statusCode: 200, response: "ok" });
            cy.get("input.password-input").type("testpassword");
            cy.contains("Kirjaudu").click();
            cy.contains("recipe1").click();
            cy.contains("Poista Resepti").click();
            cy.contains("Resepti poistettu onnistuneesti.");
            cy.contains("Ok").click();
            cy.contains("Omat suosikit");
        });
    });
});
