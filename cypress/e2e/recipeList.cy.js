import { BASE_URL } from "../support/e2e";

const filterInput = () => cy.get(".filter-input__input");
const filterItem = () => cy.get(".filter-list__item");

describe("Recipe list", () => {
    beforeEach(() => {
        cy.visit(`${BASE_URL}#/reseptit`);
    });
    it("Should display available categories", () => {
        cy.contains("Kategoriat");
        cy.contains("Kaikki");
        cy.contains("category1");
    });

    it("Should display all recipes", () => {
        cy.contains("Kaikki").click();
        cy.contains("recipe1");
        cy.contains("recipe2");
    });

    describe("Filtering", () => {
        beforeEach(() => {
            cy.visit(`${BASE_URL}#/reseptit/Kaikki`);
        });
        it("Should suggest filters", () => {
            filterInput().type("in");
            cy.contains("ingredient1");
        });

        it("Should filter recipes", () => {
            filterInput().type("in");
            cy.contains("ingredient1").click();
            filterItem().should("have.lengthOf", 1);
            cy.contains("recipe1");
            cy.contains("recipe2").should("not.exist");
        });

        it("Should clear filters", () => {
            filterInput().type("in");
            cy.contains("ingredient1").click();
            filterItem().get(".filter-list__remove-icon").click();
            filterItem().should("not.exist");
            cy.contains("recipe1");
            cy.contains("recipe2");
        });
    });
});
