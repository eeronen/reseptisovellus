import { BASE_URL } from "../support/e2e";

const portionNumber = () => cy.get(".portion-control__portion-number");
const quantityInput = () => cy.get(".ingredient-list__ingredient-quantity--input");
const favoriteButton = () => cy.get(".recipe-favorite-control");
const frontpageFavorites = () => cy.get(".landing-page-favorites__container");

describe("Recipe view", () => {
    beforeEach(() => {
        cy.visit(`${BASE_URL}#/resepti/recipe1`);
    });
    it("Should display all items", () => {
        cy.contains("recipe1");
        cy.contains("Annokset");
        cy.contains("ingredient");
        cy.contains("instruction");
    });

    it("should change portions on portion selector", () => {
        portionNumber().should("contain", 4);
        cy.contains("+").click();
        portionNumber().should("contain", 5);
        cy.contains("-").click();
        portionNumber().should("contain", 4);
    });

    it("Should change portions on recipe", () => {
        cy.contains("1 unit1");
        cy.contains("+").click();
        cy.contains("1.25 unit1");
        cy.contains("-").click();
        cy.contains("1 unit1");
    });

    it("Should open and close portion quantity input", () => {
        quantityInput().should("not.exist");
        cy.contains("unit1").click();
        quantityInput().should("exist");
        cy.contains("recipe1").click();
        quantityInput().should("not.exist");

        cy.contains("unit1").click();
        quantityInput().should("exist");
        quantityInput().type("{enter}");
        quantityInput().should("not.exist");
    });

    it("Should change portions from input", () => {
        cy.contains("unit1").click();
        quantityInput().type("1.5{enter}");
        quantityInput().should("not.exist");
        cy.contains("1.5 unit1");
        portionNumber().should("contain", 6);
    });

    it("Should add and remove recipe to favorites", () => {
        favoriteButton().contains("☆");
        favoriteButton().click();
        favoriteButton().contains("★");

        cy.visit(BASE_URL);
        frontpageFavorites().should("contain", "recipe1");

        cy.contains("recipe1").click();
        favoriteButton().contains("★");
        favoriteButton().click();
        favoriteButton().contains("☆");

        cy.visit(BASE_URL);
        frontpageFavorites().should("not.contain", "recipe1");
    });
});
