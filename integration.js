const { exec } = require("child_process");
const { spawn } = require("child_process");
require("colors");

const buildTest = () => {
    return new Promise((resolve, reject) => {
        const buildProcess = exec("npm run build-test", (error) => {
            if (error) {
                reject(error);
            }
            resolve();
        });
        buildProcess.stdout.pipe(process.stdout);
        buildProcess.stderr.pipe(process.stderr);
    });
};

const startServer = () => {
    return spawn("npm", ["run", "serve", "-s"], { shell: true });
};

const runTests = () => {
    return new Promise((resolve, reject) => {
        const testProcess = exec("npm run integration-tests", (err, stdout, stderr) => {
            if (err) {
                reject(err);
            }
            resolve();
        });
        testProcess.stdout.pipe(process.stdout);
        testProcess.stderr.pipe(process.stderr);
        return testProcess;
    });
};

(async () => {
    try {
        process.stdout.write("Building: ");
        await buildTest();
        console.log("OK".green);
        const server = startServer();
        console.log("Server started, running tests");
        server.stderr.pipe(process.stderr);
        await runTests();
        console.log("Tests completed, closing...".yellow);
        server.kill();
        process.exit();
        return;
    } catch (err) {
        console.log("FAIL".red);
        console.log(err.message);
        process.exit(1);
    }
})();
