<?php
require "utils/connection.php";
require "utils/token.php";
require "utils/clean_unused_ingredients.php";

if (!isset($_COOKIE["auth_token"]) || !check_token($_COOKIE["auth_token"])) {
    http_response_code(401);
    die("Authorization needed for editing recipes");
}

if (!isset($_POST["originalName"])) {
    http_response_code(400);
    die("Ingredient name was not set");
} elseif (!isset($_POST["newName"])) {
    http_response_code(400);
    die("New name was not set");
}

$originalName = urldecode(mb_convert_encoding($_POST["originalName"], "UTF-8"));

$ingredientIdQuery = mysqli_query($connection, "SELECT id FROM ingredients WHERE name='$originalName'");
$ingredientId = mysqli_fetch_assoc($ingredientIdQuery)["id"];

$newName = urldecode(mb_convert_encoding($_POST["newName"], "UTF-8"));

$newIngredientIdQuery = mysqli_query($connection, "SELECT id FROM ingredients WHERE name='$newName'");
$newIngredientId;
if (mysqli_num_rows($newIngredientIdQuery) > 0) {
    $newIngredientId = mysqli_fetch_assoc($newIngredientIdQuery)["id"];
    mysqli_query($connection, "UPDATE quantities SET ingredientId='$newIngredientId' WHERE ingredientId=$ingredientId");
} else {
    mysqli_query($connection, "INSERT INTO ingredients (name) VALUES ('{$newName}')");
    $newIngredientId = mysqli_insert_id($connection);
    mysqli_query($connection, "UPDATE quantities SET ingredientId='$newIngredientId' WHERE ingredientId=$ingredientId");
}

if(isset($_POST["prefix"])) {
    $prefix = urldecode(mb_convert_encoding($_POST["prefix"], "UTF-8"));
    mysqli_query($connection, "UPDATE quantities SET prefix='$prefix' WHERE ingredientId=$newIngredientId");
}
if(isset($_POST["postfix"])) {
    $postfix = urldecode(mb_convert_encoding($_POST["postfix"], "UTF-8"));
    mysqli_query($connection, "UPDATE quantities SET postfix='$postfix' WHERE ingredientId=$newIngredientId");
}

clean_unused_ingredients();
?>