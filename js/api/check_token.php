<?php
require "utils/token.php";

header('Access-Control-Allow-Origin: *');

if (isset($_COOKIE["auth_token"]) && check_token($_COOKIE["auth_token"])) {
    echo "token valid";
} else {
    http_response_code(401);
    echo "invalid token";
}
?>
