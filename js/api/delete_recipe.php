<?php
require "utils/connection.php";
require "utils/token.php";
require "utils/clean_unused_ingredients.php";

if (!isset($_COOKIE["auth_token"]) || !check_token($_COOKIE["auth_token"])) {
    http_response_code(401);
    die("Authorization needed");
} elseif (!isset($_GET["name"])) {
    http_response_code(400);
    die("name was not set");
}

$name = mb_convert_encoding($_GET["name"], "ISO-8859-1");

$recipeIdQuery = mysqli_query($connection, "SELECT id FROM recipes WHERE recipes.name='$name'");
$recipeId = mysqli_fetch_assoc($recipeIdQuery)["id"];

if (mysqli_query($connection, "DELETE FROM recipes WHERE id=$recipeId;")) {
    mysqli_query($connection, "DELETE FROM quantities WHERE recipeId=$recipeId;");
    mysqli_query($connection, "DELETE FROM instructions WHERE recipeId=$recipeId;");
    mysqli_query($connection, "DELETE FROM subtitles WHERE recipeId=$recipeId;");

    clean_unused_ingredients();
} else {
    http_response_code(500);
    echo "could not delete recipe";
}
?>