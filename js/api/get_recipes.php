<?php  require "utils/connection.php";
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$result = mysqli_query($connection, "SELECT * FROM recipes");

$allRecipes = array();

if (mysqli_num_rows($result) > 0) {
    while($recipe = mysqli_fetch_assoc($result)) {
        $ingredientsSQL = "SELECT quantity, unit, name, subtitle, postfix, prefix FROM (
            SELECT q.quantity, q.unit, i.name, NULL as subtitle, q.arrayIndex, prefix, postfix
            FROM quantities as q
            INNER JOIN ingredients as i
            ON q.ingredientId = i.id
            WHERE q.recipeId={$recipe['id']}
            UNION ALL
            SELECT NULL as quantitity, NULL as unit, NULL as name, s.subtitle, s.arrayIndex, NULL as prefix, NULL as postfix
            FROM subtitles as s
            WHERE s.recipeId={$recipe['id']}
            ) as list
            ORDER BY list.arrayIndex ASC
        ";
        $ingredients = mysqli_query($connection, $ingredientsSQL);
        
        $recipe["ingredients"] = array();
        while($ingredientsRow = mysqli_fetch_assoc($ingredients)) {
            $recipe["ingredients"][] = array_filter($ingredientsRow, static function($item){return $item !== NULL;});
        }

        $instructionsSQL = "SELECT i.instruction
            FROM instructions as i
            WHERE i.recipeId={$recipe['id']}
            ORDER BY i.arrayIndex";
        $instructions = mysqli_query($connection, $instructionsSQL);

        $recipe["instructions"] = array();
        while($instructionsRow = mysqli_fetch_assoc($instructions)) {
            $recipe["instructions"][] = $instructionsRow["instruction"];
        }
        $allRecipes[] = $recipe;
    }
}
echo json_encode($allRecipes);
?>