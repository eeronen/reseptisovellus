<?php 
require "utils/connection.php";
require "utils/token.php";

header('Access-Control-Allow-Origin: *');

$password = $_POST["password"];
$hashed = hash("sha256", $password);
$correct = "4241d4b324d18c9ad170abe0d7b2c00452541754c23e892eb5712ea0dea3586b";

if ($hashed == $correct) {
    $token = create_token();
    $sql = "INSERT INTO `auth_tokens` (`token`) VALUES ('{$token}')";
    mysqli_query($connection, $sql);
    setcookie("auth_token", $token, time() + 60*60*24*365, "/");
    echo "Log in successful";
} else {
    http_response_code(401);
    echo "login failed";
}
?>
