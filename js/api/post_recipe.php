<?php
require "utils/connection.php";
require "utils/token.php";


if (!isset($_POST["json_data"])) {
    http_response_code(400);
    die("json_data was not set");
}

$recipeData = mb_convert_encoding($_POST["json_data"], "UTF-8");
$recipe = json_decode(urldecode($recipeData));

if (!isset($recipe->name)) {
    http_response_code(400);
    die("Recipe does not have a name");
}

$sql = "INSERT INTO recipes (name, portions, cookingTime, category, picture)
    VALUES ('{$recipe->name}', '{$recipe->portions}', '{$recipe->cookingTime}', '{$recipe->category}', '{$recipe->picture}')";

if (mysqli_query($connection, $sql)) {
    $recipeId = mysqli_insert_id($connection);
    foreach($recipe->ingredients as $index=>$ingredient) {
        if (isset($ingredient->subtitle)) {
            mysqli_query(
                $connection,
                "INSERT INTO subtitles (recipeId, subtitle, arrayIndex)
                VALUES ({$recipeId}, '{$ingredient->subtitle}', {$index})"
            );
        } else {
            $ingredientResult = mysqli_query(
                $connection,
                "SELECT id FROM ingredients WHERE name='{$ingredient->name}'"
            );
            if (mysqli_num_rows($ingredientResult) > 0) {
                $ingredientId = mysqli_fetch_assoc($ingredientResult)["id"];
                mysqli_query(
                    $connection,
                    "INSERT INTO quantities (recipeId, ingredientId, quantity, unit, arrayIndex)
                    VALUES ('{$recipeId}', '{$ingredientId}', '{$ingredient->quantity}', '{$ingredient->unit}', {$index})"
                );
            } else {
                mysqli_query($connection, "INSERT INTO ingredients (name) VALUES ('{$ingredient->name}')");
                $ingredientId = mysqli_insert_id($connection);
                mysqli_query(
                    $connection,
                    "INSERT INTO quantities (recipeId, ingredientId, quantity, unit, arrayIndex)
                    VALUES ('{$recipeId}', '{$ingredientId}', '{$ingredient->quantity}', '{$ingredient->unit}', {$index})"
                );
            }
        }
    }

    foreach($recipe->instructions as $index=>$instruction) {
        mysqli_query(
            $connection,
            "INSERT INTO instructions (recipeId, instruction, arrayIndex)
            VALUES ({$recipeId}, '$instruction', {$index})"
        );
    }
}
?>