<?php
require "utils/connection.php";
require "utils/token.php";
require "utils/clean_unused_ingredients.php";

if (!isset($_COOKIE["auth_token"]) || !check_token($_COOKIE["auth_token"])) {
    http_response_code(401);
    die("Authorization needed for editing recipes");
}

if (!isset($_POST["json_data"])) {
    http_response_code(400);
    die("json_data was not set");
} elseif (!isset($_POST["name"])) {
    http_response_code(400);
    die("name was not set");
}
$recipeName = urldecode(mb_convert_encoding($_POST["name"], "UTF-8"));
$recipeData = mb_convert_encoding($_POST["json_data"], "UTF-8");
$recipe = json_decode(urldecode($recipeData));

if (!isset($recipe->name)) {
    http_response_code(400);
    die("Recipe does not have a name");
}

$recipeIdQuery = mysqli_query($connection, "SELECT id FROM recipes WHERE name='$recipeName'");

if (mysqli_num_rows($recipeIdQuery) > 0) {
    $recipeId = mysqli_fetch_assoc($recipeIdQuery)["id"];
    $sql = "UPDATE recipes SET name=$recipe->name, portions=$recipe->portions, cookingTime=$recipe->cookingTime, category=$recipe->category WHERE id=$recipeId";

    $quantities = mysqli_query($connection, "SELECT * FROM quantities WHERE recipeId=$recipeId");
    $subtitles = mysqli_query($connection, "SELECT * FROM subtitles WHERE recipeId=$recipeId");

    foreach($recipe->ingredients as $index=>$ingredient) {
        if (isset($ingredient->subtitle)) {
            $subtitleEntry = mysqli_fetch_assoc($subtitles);
            if ($subtitleEntry == NULL) {
                mysqli_query(
                    $connection,
                    "INSERT INTO subtitles (recipeId, subtitle, arrayIndex)
                    VALUES ({$recipeId}, '{$ingredient->subtitle}', {$index})"
                );
            } else {
                mysqli_query(
                    $connection,
                    "UPDATE subtitles 
                    SET subtitle='$ingredient->subtitle', arrayIndex=$index
                    WHERE id={$subtitleEntry['id']}"
                );
            }
        } else {
            $ingredientResult = mysqli_query(
                $connection,
                "SELECT id FROM ingredients WHERE name='{$ingredient->name}'"
            );
            
            $ingredientId;
            if (mysqli_num_rows($ingredientResult) > 0) {
                $ingredientId = mysqli_fetch_assoc($ingredientResult)["id"];
            } else {
                mysqli_query($connection, "INSERT INTO ingredients (name) VALUES ('{$ingredient->name}')");
                $ingredientId = mysqli_insert_id($connection);
            }
            
            $quantityEntry = mysqli_fetch_assoc($quantities);
            if ($quantityEntry == NULL) {
                mysqli_query(
                    $connection,
                    "INSERT INTO quantities (recipeId, ingredientId, quantity, unit, arrayIndex)
                    VALUES ('{$recipeId}', '{$ingredientId}', '{$ingredient->quantity}', '{$ingredient->unit}', $index)"
                );
            } else {
                mysqli_query(
                    $connection,
                    "UPDATE quantities 
                    SET ingredientId=$ingredientId, quantity={$ingredient->quantity}, unit='{$ingredient->unit}', arrayIndex=$index
                    WHERE id={$quantityEntry['id']}"
                );
            }
        }
    }

    while($extraQuantity = mysqli_fetch_assoc($quantities)) {
        mysqli_query($connection, "DELETE FROM quantities WHERE id={$extraQuantity['id']}");
    }
    while($extraSubtitle = mysqli_fetch_assoc($subtitles)) {
        mysqli_query($connection, "DELETE FROM subtitles WHERE id={$extraSubtitle['id']}");
    }

    $instructions = mysqli_query($connection, "SELECT * FROM instructions WHERE recipeId=$recipeId");
    foreach($recipe->instructions as $index=>$instruction) {
        $instructionEntry = mysqli_fetch_assoc($instructions);
        if ($instructionEntry == NULL) {
            mysqli_query(
                $connection,
                "INSERT INTO instructions (recipeId, instruction, arrayIndex)
                VALUES ({$recipeId}, '$instruction', {$index})"
            );
        } else {
            mysqli_query(
                $connection,
                "UPDATE instructions
                SET instruction='$instruction', arrayIndex=$index
                WHERE id={$instructionEntry['id']}"
            );
        }
    }
    while($extraInstruction = mysqli_fetch_assoc($instructions)) {
        mysqli_query($connection, "DELETE FROM instructions WHERE id={$extraInstruction['id']}");
    }

    clean_unused_ingredients();
} else {
    echo "No recipe found";
}
?>