<?php
require "../utils/connection.php";
require "../utils/token.php";

header('Access-Control-Allow-Origin: *');

if (!isset($_COOKIE["auth_token"]) || !check_token($_COOKIE["auth_token"])) {
    http_response_code(401);
    die("Authorization needed");
} elseif (!isset($_GET["name"])) {
    http_response_code(400);
    die("You must set recipe name as url parameter");
}

$name = mb_convert_encoding($_GET["name"], "UTF-8");

$recipeName = mb_convert_encoding($name, "UTF-8");

$img = $_FILES["recipeImage"];
$imageFileType = strtolower(pathinfo($img["name"],PATHINFO_EXTENSION));
$image_file_name = uniqid() . "." . $imageFileType;
$target_file = $image_file_name;

$check = getimagesize($img["tmp_name"]);
if(!$check && $imageFileType != "jpeg" && $imageFileType != "png" && $imageFileType != "jpg") {
    die("File is not an image.");
} elseif (file_exists($target_file)) {
    die("File already exists.");
}

if (move_uploaded_file($img["tmp_name"], $target_file)) {
    echo "The file ". basename( $img["name"]). " has been uploaded.";
} else {
    echo "Sorry, there was an error uploading your file.";
}
$sql_for_old_photo = "SELECT picture FROM recipes WHERE name = '{$name}'";
$old_photo_query = mysqli_query($connection, $sql_for_old_photo);
$old_photo_name = mysqli_fetch_assoc($old_photo_query)["kuva"];

$sql = "UPDATE recipes SET picture='{$image_file_name}' WHERE `name`='{$recipeName}'";


if (mysqli_query($connection, $sql)) {
    echo "recipe updated successfully";

    if ($old_photo_name != NULL && file_exists($old_photo_name)) {
        unlink($old_photo_name);
    }
} else {
    http_response_code(500);
    echo "could not add to database";
}
?>