<?php
function clean_unused_ingredients() {
    require "connection.php";
    $ingredientIds = mysqli_query($connection, "SELECT id FROM ingredients WHERE 1");
    while($ingredient = mysqli_fetch_assoc($ingredientIds)) {
        $ingredientId = $ingredient["id"];
        $ingredientUsages = mysqli_query($connection, "SELECT * FROM quantities WHERE quantities.ingredientId=$ingredientId");
        if (mysqli_num_rows($ingredientUsages) == 0) {
            mysqli_query($connection, "DELETE FROM ingredients WHERE id=$ingredientId");
        }
    }
}
?>