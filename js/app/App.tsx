import * as React from "react";
import { HashRouter, Route, Routes, useParams } from "react-router-dom";
import { Menu } from "./components/menu/menu";
import { CategoryList } from "./components/listViews/categoryList";
import { RecipeList } from "./components/listViews/recipeList";
import { RecipeEditing } from "./components/recipeEditing/recipeEditing";
import { ManageRecipes } from "./components/listViews/manageRecipesView";
import { PrivateRoute } from "./utils/PrivateRoute";
import { LandingPage } from "./components/landingPage/landingPage";
import { showErrorDialog } from "./commonComponents/modalDialog";
import { Provider } from "react-redux";
import { store } from "./state";
import { getRecipes } from "./services/recipeservice";
import { RecipeView } from "./components/recipeView/RecipeView";
import { initializeColorTheme } from "./utils/colorThemeUtils";
import { Search } from "./components/menu/search";
import { recipesActions } from "./state/recipes";

initializeColorTheme();

window.onerror = (msg) => {
    console.error(msg);
    showErrorDialog({ header: "Tuntematon virhe", message: msg.toString() });
};
getRecipes().then((recipes) => {
    store.dispatch(recipesActions.setAllRecipes(recipes));
});

export const App: React.FC<{}> = () => (
    <HashRouter>
        <div className="main-content">
            <Provider store={store}>
                <Menu />
                <Search />
                <div className="content-container">
                    <Routes>
                        <Route path="/" element={<LandingPage />} />
                        <Route path="/reseptit" element={<CategoryList />} />
                        <Route path="/reseptit/:category" element={<RecipeListComponent />} />
                        <Route path="/resepti/:recipeName" element={<RecipeViewComponent />} />
                        <Route
                            path="/reseptin-muokkaus/:recipeName"
                            element={<RecipeEditingComponent />}
                        />
                        <Route
                            path="/hallitse"
                            element={<PrivateRoute element={<ManageRecipes />} />}
                        />
                    </Routes>
                </div>
            </Provider>
        </div>
    </HashRouter>
);

const RecipeListComponent: React.FC<{}> = () => {
    const params = useParams();

    return <RecipeList category={params.category!} />;
};

const RecipeViewComponent: React.FC<{}> = () => {
    const params = useParams();
    return <RecipeView recipeName={params.recipeName!} />;
};

const RecipeEditingComponent: React.FC<{}> = () => {
    const params = useParams();
    return <RecipeEditing recipeName={params.recipeName!} />;
};
