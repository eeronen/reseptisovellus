import * as React from "react";

export const LoadingSpinner: React.FC<{}> = () => {
    return <div className="loading-spinner" />;
};
