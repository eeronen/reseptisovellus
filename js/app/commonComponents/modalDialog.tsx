import * as React from "react";
import * as ReactDOM from "react-dom";
import { createRoot } from "react-dom/client";

interface IGenericDialog {
    header?: string;
    message?: string;
}

export const modalRoot = createRoot(document.getElementById("modal-root") as HTMLElement);

export const showErrorDialog = (error: IGenericDialog) => {
    const errorHeader = error.header || "Otsikko";
    const errorMessage = error.message || "Tuntematon virhe";

    modalRoot.render(<ErrorDialog header={errorHeader} message={errorMessage} />);
};

export const hideDialog = () => {
    modalRoot.render(null);
};

const ErrorDialog: React.FC<{ header: string; message: string }> = (props) => {
    return (
        <div className="modal-dialog__background">
            <div className="error-dialog">
                <div className="error-dialog__header">{props.header}</div>
                <div className="error-dialog__message">{props.message}</div>
                <div className="error-dialog__footer">
                    <div className="error-dialog__footer__buttons">
                        <button className="error-dialog__primary-button" onClick={hideDialog}>
                            Sulje
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

interface IModalDialog extends IGenericDialog {
    primaryAction: () => void;
}
export const showModalDialog = (props: IModalDialog) => {
    const headerText = props.header || "Imoitus";
    const messageText = props.message || "";
    const primaryAction = props.primaryAction || (() => {});

    modalRoot.render(
        <ModalDialog header={headerText} message={messageText} primaryAction={primaryAction} />
    );
};

const ModalDialog: React.FC<{
    header: string;
    message: string;
    primaryAction: () => void;
}> = (props) => {
    const onPrimaryClick = () => {
        props.primaryAction();
        hideDialog();
    };
    return (
        <div className="modal-dialog__background">
            <div className="modal-dialog">
                <div className="modal-dialog__header">{props.header}</div>
                <div className="modal-dialog__message">{props.message}</div>
                <div className="modal-dialog__footer">
                    <div className="modal-dialog__footer__buttons">
                        <button className="modal-dialog__primary-button" onClick={onPrimaryClick}>
                            Ok
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export const Modal: React.FC<
    React.PropsWithChildren<{
        header?: string;
        primaryButton?: { action: () => void; text: string };
    }>
> = (props) => {
    const modalRoot = document.getElementById("modal-root");
    const el = document.createElement("div");
    modalRoot!.appendChild(el);

    React.useEffect(() => {
        return () => {
            modalRoot!.removeChild(el);
        };
    }, []);

    if (props.header) {
        return ReactDOM.createPortal(
            <div className="modal-dialog__background">
                <div className="modal-dialog">
                    <div className="modal-dialog__header">{props.header}</div>
                    <div className="modal-dialog__message">{props.children}</div>
                    {props.primaryButton && (
                        <div className="modal-dialog__footer">
                            <div className="modal-dialog__footer__buttons">
                                <button
                                    className="modal-dialog__primary-button"
                                    onClick={props.primaryButton.action}
                                >
                                    {props.primaryButton.text}
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            </div>,
            el
        );
    } else {
        return ReactDOM.createPortal(
            <div className="modal-dialog__background">
                <div className="modal-dialog">{props.children}</div>
            </div>,
            el
        );
    }
};
