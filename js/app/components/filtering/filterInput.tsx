import * as React from "react";
import { useAppDispatch, useAppSelector, selectors } from "../../state";
import { viewStateAction } from "../../state/viewState";
import { reduceIngredients } from "./utils/filteringFunctions";

export const FilterInput: React.FC<{}> = () => {
    const [inputValue, setInputValue] = React.useState("");

    const dispatch = useAppDispatch();

    const addFilter = React.useCallback(
        (filter) => dispatch(viewStateAction.addFilter(filter)),
        []
    );

    const ingredients = useAppSelector((state) =>
        reduceIngredients(selectors.getAllIngredientNames(state))
    );

    return (
        <div className="filter-input__container">
            <input
                type="text"
                name="Filter"
                className="filter-input__input"
                placeholder="Syötä raaka-aine"
                value={inputValue}
                onChange={(event) => setInputValue(event.target.value)}
            />
            {inputValue.length !== 0 && (
                <div className="filter-input__list-container">
                    <ul className="filter-input__list">
                        {getSuggestions(ingredients, inputValue).map((suggestion) => (
                            <li
                                key={suggestion}
                                className="filter-input__list-item"
                                onClick={() => {
                                    addFilter(suggestion);
                                    setInputValue("");
                                }}
                            >
                                {suggestion}
                            </li>
                        ))}
                    </ul>
                </div>
            )}
        </div>
    );
};

export const getSuggestions = (ingredients: string[], searchValue: string): string[] => {
    return ingredients.filter((ingredient) =>
        ingredient.toLowerCase().includes(searchValue.toLowerCase())
    );
};
