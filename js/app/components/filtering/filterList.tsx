import * as React from "react";
import { selectors, useAppDispatch, useAppSelector } from "../../state";
import { viewStateAction } from "../../state/viewState";

export const FilterList: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const removeFilter = React.useCallback(
        (filter) => () => dispatch(viewStateAction.removeFilter(filter)),
        []
    );
    const filters = useAppSelector((state) => selectors.getAppliedFilters(state));

    return (
        <div className="filter-list__container">
            {filters.map((filter) => (
                <div className="filter-list__item" key={filter}>
                    {filter}
                    <div className="filter-list__remove" onClick={removeFilter(filter)}>
                        <div className="filter-list__remove-icon">x</div>
                    </div>
                </div>
            ))}
        </div>
    );
};
