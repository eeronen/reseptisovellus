import * as React from "react";
import { Link } from "react-router-dom";
import { filterRecipes } from "./utils/filteringFunctions";
import { LoadingSpinner } from "../../commonComponents/loadSpinner";

import { FilterInput } from "./filterInput";
import { FilterList } from "./filterList";
import { selectors, useAppSelector } from "../../state";

export const RecipeFiltering: React.FC<{}> = () => {
    const isLoaded = useAppSelector((state) => selectors.isRecipesLoaded(state));

    if (isLoaded) {
        return (
            <div className="filter-content">
                <FilterList />
                <FilterInput />
                <div className="filtered-recipes">
                    <FilteredRecipes />
                </div>
            </div>
        );
    } else {
        return <LoadingSpinner />;
    }
};

export const FilteredRecipes: React.FC<{}> = () => {
    const appliedFilters = useAppSelector((state) => selectors.getAppliedFilters(state));
    const filteredRecipes = useAppSelector((state) =>
        filterRecipes(appliedFilters, selectors.getAllRecipes(state))
    );

    return (
        <ul className="filtered-recipes__list">
            {filteredRecipes.length !== 0 ? (
                filteredRecipes.map((recipeName) => (
                    <li className="filtered-recipes__list-item" key={recipeName}>
                        <Link to={"/resepti/" + recipeName} className="filtered-recipes__link">
                            {recipeName}
                        </Link>
                    </li>
                ))
            ) : (
                <div className="filtered-recipes__list--no-results">Ei reseptejä</div>
            )}
        </ul>
    );
};
