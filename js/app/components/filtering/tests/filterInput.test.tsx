import "mocha";
import * as React from "react";
import { fake, restore } from "sinon";
import { shallow } from "enzyme";

import { FilterInput, getSuggestions } from "../filterInput";
import { TEST_RECIPE } from "../../../../../test/testRecipe";
import { expect } from "chai";
import { removeDuplicateValues } from "../utils/filteringFunctions";
import { fakeReactUseState } from "../../../utils/testingUtils";

const TEST_INGREDIENTS = removeDuplicateValues(
    TEST_RECIPE.reduce(
        (ingredients, recipe) =>
            ingredients.concat(recipe.ingredients.map(ingredient => ingredient.name)),
        [] as string[]
    )
);

describe("filterInput", () => {
    afterEach(() => {
        restore();
    });

    it("Should change filter input", () => {
        const setState = fakeReactUseState("");
        const wrapper = shallow(
            <FilterInput ingredients={TEST_INGREDIENTS} filterAdded={() => undefined} />
        );
        wrapper.find("input").simulate("change", { target: { value: "foobar" } });
        expect(setState.lastCall.calledWith("foobar"));
    });

    it("Should add filter and clear input", () => {
        const setState = fakeReactUseState("foo");

        const filterAddedFunction = fake();
        const wrapper = shallow(
            <FilterInput ingredients={TEST_INGREDIENTS} filterAdded={filterAddedFunction} />
        );
        wrapper.find("input").simulate("change", { target: { value: "foo" } });
        expect(setState.lastCall.calledWith("foo"));

        wrapper
            .find(".filter-input__list-item")
            .first()
            .simulate("click");
        expect(setState.lastCall.calledWith(""));
        expect(filterAddedFunction.called).to.be.true;
    });

    it("Should return suggestions", () => {
        expect(getSuggestions(TEST_INGREDIENTS, "F")).to.have.members(["foo"]);
    });
});
