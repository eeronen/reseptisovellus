import "mocha";
import * as React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";

import { FilterList } from "../filterList";
import { stub } from "sinon";

describe("filterLists", () => {
    it("Should render all components", () => {
        const wrapper = shallow(
            <FilterList filters={["foo", "bar"]} removeFilter={() => undefined} />
        );
        expect(wrapper.find(".filter-list__item")).to.have.lengthOf(2);
    });

    it("Should render empty list", () => {
        const wrapper = shallow(<FilterList filters={[]} removeFilter={() => undefined} />);
        expect(wrapper.find(".filter-list__item")).to.be.empty;
    });

    it("Should call remove filter function", () => {
        const removeFunction = stub();
        const wrapper = shallow(
            <FilterList filters={["foo", "bar"]} removeFilter={removeFunction} />
        );
        wrapper
            .find(".filter-list__remove")
            .first()
            .simulate("click");
        expect(removeFunction.called).to.be.true;
    });
});
