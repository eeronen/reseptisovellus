import "mocha";
import { expect } from "chai";

import {
    reduceIngredients,
    filterRecipes,
    findSubstitute,
    stripIngredientName,
    removeDuplicateValues,
} from "../utils/filteringFunctions";
import { TEST_RECIPE } from "../../../../../test/testRecipe";

describe("filteringFunctions", () => {
    it("Should strip ingredient name", () => {
        expect(stripIngredientName("hienonnettua korianteria")).to.equal("korianteria");
        expect(stripIngredientName("korianteria pilkottuna")).to.equal("korianteria");
        expect(stripIngredientName("hienonnettua korianteria pilkottuna")).to.equal("korianteria");
    });

    it("Should reduce ingredients", () => {
        const ingredients = [
            "hienonnettua korianteria",
            "korianteria",
            "tomaattia",
            "tomaattia pilkottuna",
            "tomaattia",
        ];
        const reduced = reduceIngredients(ingredients);
        expect(reduced).to.have.lengthOf(2);
        expect(reduced).to.have.same.members(["korianteria", "tomaattia"]);
    });

    it("Should remove duplicate ingredients", () => {
        expect(removeDuplicateValues(["foo", "bar", "foo", "bar", "foo"])).to.have.lengthOf(2);
    });

    it("Should find substitutes", () => {
        expect(findSubstitute("kevätsipulia")).to.be.equal("sipulia");
        expect(findSubstitute("sipulia")).to.be.equal("sipulia");
        expect(findSubstitute("keltaista paprikaa")).to.be.equal("paprikaa");
    });

    it("Should filter recipes", () => {
        const recipes = TEST_RECIPE;
        const filters = ["foo"];
        expect(filterRecipes(filters, recipes)).to.have.lengthOf(2);
        expect(filterRecipes(filters, recipes)).to.have.same.members(["recipe1", "recipe2"]);
    });
});
