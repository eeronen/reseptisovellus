import "mocha";
import * as React from "react";
import { shallow } from "enzyme";
import { RecipeFiltering, FilteredRecipes, RecipeFilteringProps } from "../recipeFiltering";
import { expect } from "chai";
import { FilterList } from "../filterList";
import { FilterInput } from "../filterInput";
import { LoadingSpinner } from "../../../commonComponents/loadSpinner";
import { TEST_RECIPE } from "../../../../../test/testRecipe";
import { Link } from "react-router-dom";

const INGREDIENTS = ["foo", "bar"];

const defaultRecipeFilteringProps: RecipeFilteringProps = {
    recipes: TEST_RECIPE,
    ingredientList: INGREDIENTS,
    appliedFilters: [],
    isLoaded: true,
    addFilter: _ => {},
    removeFilter: _ => () => {},
    clearFilters: _ => {},
};

describe("recipeFiltering", () => {
    it("Should render loading spinner", () => {
        const wrapper = shallow(
            <RecipeFiltering {...{ ...defaultRecipeFilteringProps, isLoaded: false }} />
        );
        expect(wrapper.find(LoadingSpinner)).to.have.lengthOf(1);
        expect(wrapper.find(FilterInput)).to.have.lengthOf(0);
    });

    it("Should render all components", () => {
        const wrapper = shallow(<RecipeFiltering {...defaultRecipeFilteringProps} />);
        expect(wrapper.find(FilterList)).to.have.lengthOf(1);
        expect(wrapper.find(FilterInput)).to.have.lengthOf(1);
        expect(wrapper.find(FilteredRecipes)).to.have.lengthOf(1);
    });

    it("Should reset filters");

    it("Should remove filter");

    it("Should get filters with substitutes");
});

describe("filteredRecipes", () => {
    it("Should render all matching recipes", () => {
        const wrapper = shallow(<FilteredRecipes recipes={TEST_RECIPE} filters={["foo"]} />);
        expect(wrapper.find(Link)).to.have.lengthOf(2);
    });
    it("Should render no recipes", () => {
        const wrapper = shallow(
            <FilteredRecipes recipes={TEST_RECIPE} filters={["nonexistent"]} />
        );
        expect(wrapper.find(Link)).to.have.lengthOf(0);
        expect(wrapper.find(".filtered-recipes__list--no-results")).to.have.lengthOf(1);
    });
});
