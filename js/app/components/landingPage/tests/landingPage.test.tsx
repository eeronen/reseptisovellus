import "mocha";
import * as React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";

import { FavoriteList, HistoryList } from "../landingPage";

describe("landingPage", () => {
    it("Should render empty favorites", () => {
        const wrapper = shallow(<FavoriteList favorites={[]} />);
        expect(wrapper.find(".landing-page-favorites__list--no-content")).to.have.lengthOf(1);
        expect(wrapper.find(".landing-page-favorites__list__item")).to.have.lengthOf(0);
    });

    it("Should render favorites", () => {
        const wrapper = shallow(<FavoriteList favorites={["foo", "bar", "baz"]} />);
        expect(wrapper.find(".landing-page-favorites__list--no-favorites")).to.have.lengthOf(0);
        expect(wrapper.find(".landing-page-favorites__item")).to.have.lengthOf(3);
    });

    it("Should render empty history", () => {
        const wrapper = shallow(<HistoryList history={[]} />);
        expect(wrapper.find(".landing-page-history__list--no-content")).to.have.lengthOf(1);
        expect(wrapper.find(".landing-page-history__item")).to.have.lengthOf(0);
    });

    it("Should render history", () => {
        const wrapper = shallow(<HistoryList history={["foo", "bar", "baz"]} />);
        expect(wrapper.find(".landing-page-history__list--no-history")).to.have.lengthOf(0);
        expect(wrapper.find(".landing-page-history__item")).to.have.lengthOf(3);
    });
});
