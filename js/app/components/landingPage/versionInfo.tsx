import * as React from "react";
import {
    isLatestVersionSeen,
    markLatestVersionAsSeen,
    getNewFeaturesSince,
    getLastSeenVersion,
} from "../../utils/versionUtils";

export const VersionInfo: React.FC<{}> = () => {
    const [isLatestSeen, setLatestSeen] = React.useState(isLatestVersionSeen());

    if (!isLatestSeen) {
        return (
            <div className="version-info">
                <VersionInfoHeader />
                <VersionInfoBody />
                <button
                    className="version-info__ok-button"
                    onClick={() => {
                        markLatestVersionAsSeen();
                        setLatestSeen(true);
                    }}
                >
                    Asia selvä!
                </button>
            </div>
        );
    } else {
        return null;
    }
};

const VersionInfoHeader: React.FC<{}> = () => (
    <div className="version-info__header">Reseptisovellusta on päivitetty</div>
);

const VersionInfoBody: React.FC<{}> = () => {
    const features = getNewFeaturesSince(getLastSeenVersion());
    return (
        <div className="version-info__body">
            <div className="version-info__text">Uutta tässä versiossa:</div>
            <ul className="version-info__feature-list">
                {features.map((feature, index) => (
                    <li key={index} className="version-info__feature-list-item">
                        {feature}
                    </li>
                ))}
            </ul>
        </div>
    );
};
