import * as React from "react";
import { Link } from "react-router-dom";
import { LoadingSpinner } from "../../commonComponents/loadSpinner";
import { selectors, useAppSelector } from "../../state";

export const CategoryList: React.FC<{}> = () => {
    const categories = useAppSelector((state) => selectors.getCategories(state));
    const isLoaded = useAppSelector((state) => selectors.isRecipesLoaded(state));
    if (isLoaded) {
        return (
            <div>
                <div className="category-list__header">Kategoriat</div>
                <ul className="category-list">
                    <CategoryListItems categories={categories} />
                </ul>
            </div>
        );
    } else {
        return <LoadingSpinner />;
    }
};

export const CategoryListItems: React.FC<{ categories: string[] }> = (props) => (
    <>
        {props.categories.map((category, i) => (
            <li className="category-item" key={i}>
                <Link to={"/reseptit/" + category} className="category-item__link">
                    {category}
                </Link>
            </li>
        ))}
    </>
);
