import * as React from "react";
import { Link } from "react-router-dom";
import { selectors, useAppSelector } from "../../state";
import { LoadingSpinner } from "../../commonComponents/loadSpinner";

export const ManageRecipes: React.FC<{}> = () => {
    const recipes = useAppSelector((state) => selectors.getAllRecipeNames(state));
    const isLoaded = useAppSelector((state) => selectors.isRecipesLoaded(state));
    if (!isLoaded) {
        return <LoadingSpinner />;
    } else {
        return (
            <ul className="recipe-list">
                <ManageRecipesListItems recipes={recipes} />
            </ul>
        );
    }
};

export const ManageRecipesListItems: React.FC<{ recipes: string[] }> = (props) => (
    <>
        {props.recipes.map((name, i) => (
            <li className="recipelist-item" key={i}>
                <Link to={"/reseptin-muokkaus/" + name} className="recipelist-item__link">
                    {name}
                </Link>
            </li>
        ))}
    </>
);
