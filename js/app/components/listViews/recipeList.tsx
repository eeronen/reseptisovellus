import * as React from "react";
import { Link } from "react-router-dom";
import { LoadingSpinner } from "../../commonComponents/loadSpinner";
import { RecipeFiltering } from "../../components/filtering/recipeFiltering";
import { selectors, useAppSelector } from "../../state";

export const RecipeList: React.FC<{
    category: string;
}> = ({ category }) => {
    const recipes = useAppSelector((state) => selectors.getRecipesWithCategory(state, category));
    const isLoaded = useAppSelector((state) => selectors.isRecipesLoaded(state));
    if (category === "Kaikki") {
        return <RecipeFiltering />;
    } else if (!isLoaded) {
        return <LoadingSpinner />;
    } else {
        return (
            <div>
                <div className="recipe-list__header">{category}</div>
                <ul className="recipe-list">
                    <RecipeListItems recipes={recipes} />
                </ul>
            </div>
        );
    }
};

export const RecipeListItems: React.FC<{ recipes: string[] }> = (props) => (
    <>
        {props.recipes.map((recipe, i) => (
            <li className="recipelist-item" key={i}>
                <Link to={"/resepti/" + recipe} className="recipelist-item__link">
                    {recipe}
                </Link>
            </li>
        ))}
    </>
);
