import "mocha";
import * as React from "react";
import { shallow } from "enzyme";
import { CategoryList, CategoryListItems } from "../categoryList";
import { expect } from "chai";
import { LoadingSpinner } from "../../../commonComponents/loadSpinner";
import { Link } from "react-router-dom";

describe("categoryList", () => {
    it("Should render correctly", () => {
        const wrapper1 = shallow(<CategoryList isLoaded={false} categories={[]} />);
        expect(wrapper1.find(LoadingSpinner)).to.have.lengthOf(1);

        const wrapper2 = shallow(<CategoryList isLoaded={true} categories={["category1"]} />);

        expect(wrapper2.find(LoadingSpinner)).to.have.lengthOf(0);
        expect(wrapper2.find(CategoryListItems)).to.have.lengthOf(1);
    });

    it("Should have links", () => {
        const wrapper = shallow(<CategoryListItems categories={["foo", "bar"]} />);
        expect(wrapper.find(Link)).to.have.lengthOf(2);
    });
});
