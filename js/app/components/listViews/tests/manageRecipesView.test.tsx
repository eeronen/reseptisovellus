import "mocha";
import * as React from "react";
import { shallow } from "enzyme";
import { expect } from "chai";
import { TEST_RECIPE } from "../../../../../test/testRecipe";
import { Link } from "react-router-dom";
import { ManageRecipes, ManageRecipesListItems } from "../manageRecipesView";

describe("manageRecipesView", () => {
    it("Should render correctly", () => {
        const wrapper = shallow(<ManageRecipes isLoaded={true} recipes={["foo", "bar"]} />);
        expect(wrapper.find(ManageRecipesListItems)).to.have.lengthOf(1);
    });
    it("Should have links", () => {
        const wrapper = shallow(
            <ManageRecipesListItems recipes={TEST_RECIPE.map(recipe => recipe.name)} />
        );
        expect(wrapper.find(Link)).to.have.lengthOf(3);
    });
});
