import "mocha";
import * as React from "react";
import { shallow } from "enzyme";
import { expect } from "chai";
import { LoadingSpinner } from "../../../commonComponents/loadSpinner";
import { Link } from "react-router-dom";
import { RecipeList, RecipeListItems } from "../recipeList";
import { RecipeFiltering } from "../../filtering/recipeFiltering";

describe("recipeList", () => {
    it("Should render correctly", () => {
        const wrapper = shallow(
            <RecipeList recipes={["foo", "bar"]} isLoaded={true} category={"category"} />
        );
        expect(wrapper.find(LoadingSpinner)).to.have.lengthOf(0);
        expect(wrapper.find(RecipeListItems)).to.have.lengthOf(1);
    });

    it("Should render filtered recipes", () => {
        const wrapper = shallow(
            <RecipeList category="Kaikki" recipes={["foo", "bar"]} isLoaded={true} />
        );
        expect(wrapper.find(RecipeFiltering)).to.have.lengthOf(1);
    });

    it("Should have links", () => {
        const wrapper = shallow(<RecipeListItems recipes={["foo", "bar", "baz"]} />);
        expect(wrapper.find(Link)).to.have.lengthOf(3);
    });
});
