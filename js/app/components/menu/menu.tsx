import * as React from "react";
// @ts-ignore
import * as SearchIcon from "../../../assets/icon-search.inline.svg";
import { NavigationIcon, NavigationLinks } from "./navigationLinks";
import { Tabs } from "./tabs";
import { useAppDispatch } from "../../state";
import { appActions } from "../../state/appState";

export const Menu: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const expandSearch = React.useCallback(
        () => dispatch(appActions.setSearchExpanded(true)),
        [dispatch]
    );
    return (
        <header className="">
            <nav className="navigation-bar">
                <NavigationIcon />
                <NavigationLinks />
                <Tabs />
                <div className="icon search__icon" onClick={expandSearch}>
                    <SearchIcon />
                </div>
            </nav>
        </header>
    );
};
