import * as React from "react";

//@ts-ignore
import * as CloseIcon from "../../../assets/icon-close.inline.svg";
//@ts-ignore
import * as MenuIcon from "../../../assets/icon-menu.inline.svg";
//@ts-ignore
import * as Logo from "../../../assets/icon-main.inline.svg";
//@ts-ignore
import * as RecipesIcon from "../../../assets/icon-cutlery.inline.svg";
//@ts-ignore
import * as AddRecipeIcon from "../../../assets/icon-plus.inline.svg";
//@ts-ignore
import * as EditRecipesIcon from "../../../assets/icon-edit.inline.svg";
import { ColorThemePicker } from "./colorThemePicker";
import { selectors, useAppDispatch, useAppSelector } from "../../state";
import { appActions } from "../../state/appState";

const NAVIGATION_LINKS = [
    {
        name: "Etusivu",
        icon: Logo,
        url: "#/",
    },
    {
        name: "Reseptit",
        icon: RecipesIcon,
        url: "#/reseptit",
    },
    {
        name: "Uusi resepti",
        icon: AddRecipeIcon,
        url: "#/reseptin-muokkaus/uusi",
    },
    {
        name: "Hallitse reseptejä",
        icon: EditRecipesIcon,
        url: "#/hallitse",
    },
];

export const NavigationIcon: React.FC<{}> = () => {
    const dispatch = useAppDispatch();

    const expandMenu = React.useCallback(
        () => dispatch(appActions.setMenuExpanded(true)),
        [dispatch]
    );

    return (
        <div className="icon navigation-bar__menu-icon" onClick={expandMenu}>
            <MenuIcon />
        </div>
    );
};

export const NavigationLinks: React.FC<{}> = () => {
    const dispatch = useAppDispatch();

    const isExpanded = useAppSelector((state) => selectors.isMenuExpanded(state));

    const collapseMenu = React.useCallback(
        () => dispatch(appActions.setMenuExpanded(false)),
        [dispatch]
    );

    return (
        <ul className={`navigation-bar__link-container ${isExpanded ? "expanded" : ""}`}>
            <li className="navigation-bar__close-icon" onClick={collapseMenu}>
                <CloseIcon />
            </li>
            {NAVIGATION_LINKS.map((link) => (
                <li onClick={collapseMenu} key={link.name}>
                    <a className="navigation-bar__link" href={link.url} key={link.name}>
                        {link.name}
                    </a>
                </li>
            ))}
            <ColorThemePicker />
        </ul>
    );
};
