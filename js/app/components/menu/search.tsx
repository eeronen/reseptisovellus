import * as React from "react";

// @ts-ignore
import * as SearchIcon from "../../../assets/icon-search.inline.svg";
import { selectors, useAppDispatch, useAppSelector } from "../../state";
import { SearchResults } from "./searchResults";
import { appActions } from "../../state/appState";

export const Search: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const searchInput = React.createRef<HTMLInputElement>();

    const searchValue = useAppSelector((state) => selectors.getSearchValue(state));
    const isExpanded = useAppSelector((state) => selectors.isSearchExpanded(state));

    const onSearchChange = React.useCallback(
        (event: React.ChangeEvent<HTMLInputElement>) => {
            dispatch(appActions.setSearchValue(event.target.value));
        },
        [dispatch]
    );
    const collapseSearch = React.useCallback(
        () => dispatch(appActions.setSearchExpanded(false)),
        [dispatch]
    );
    React.useEffect(() => {
        if (isExpanded && searchInput.current !== null) {
            searchInput.current.focus();
        }
    }, [isExpanded]);

    return (
        <div className={`search search--${isExpanded ? "expanded" : "collapsed"}`}>
            <div className="search__header">
                <input
                    className="search__input"
                    value={searchValue}
                    onChange={onSearchChange}
                    placeholder="Hae reseptejä..."
                    ref={searchInput}
                />
                <button className="search__close-button" onClick={collapseSearch}>
                    Sulje
                </button>
            </div>
            <SearchResults toggleExpanded={collapseSearch} />
        </div>
    );
};
