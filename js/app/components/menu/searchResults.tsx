import * as React from "react";
import { Link } from "react-router-dom";
import { selectors, useAppSelector } from "../../state";

export const SearchResults: React.FC<{ toggleExpanded: () => void }> = ({ toggleExpanded }) => {
    const searchValue = useAppSelector((state) => selectors.getSearchValue(state));
    const recipeNames = useAppSelector((state) => selectors.getAllRecipeNames(state));
    if (searchValue === "") {
        return null;
    } else {
        return (
            <ul className="search__results-list">
                {recipeNames
                    .filter((recipe) => {
                        return recipe.toLowerCase().includes(searchValue.toLowerCase());
                    })
                    .map((result) => (
                        <li className="search__result-item" key={result}>
                            <Link to={`/resepti/${result}`} onClick={toggleExpanded}>
                                {result}
                            </Link>
                        </li>
                    ))}
            </ul>
        );
    }
};
