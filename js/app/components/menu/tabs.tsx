import * as React from "react";

import { selectors, useAppSelector } from "../../state";
import { Link } from "react-router-dom";

export const Tabs: React.FC<{}> = () => {
    const tabs = useAppSelector((state) => selectors.getTabs(state));

    return (
        <div className="tabs__container">
            {tabs.map((tab) => (
                <Link to={"/resepti/" + tab} className="tabs__tab">
                    {tab}
                </Link>
            ))}
        </div>
    );
};
