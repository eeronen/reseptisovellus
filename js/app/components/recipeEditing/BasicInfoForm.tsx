import * as React from "react";
import { CATEGORIES } from "../../constants";
import { noSpecialChars, notEmpty, isNumeric, isPositive } from "./FormValidators";
import { ValidatedInput } from "./validatedInput";
import { getEditableRecipe } from "../../state/selectors";
import { useAppDispatch, useAppSelector } from "../../state";
import { handleInputChange } from "../../utils";
import { editRecipeActions } from "../../state/editState/currentRecipe";

export const BasicInfoForm: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const recipe = useAppSelector((state) => getEditableRecipe(state));

    const setName = React.useCallback(
        (event) =>
            handleInputChange(event, (name) =>
                dispatch(editRecipeActions.setRecipeName(name as string))
            ),
        [dispatch]
    );
    const setCategory = React.useCallback(
        (event) =>
            handleInputChange(event, (category) =>
                dispatch(editRecipeActions.setCategory(category as string))
            ),
        [dispatch]
    );
    const setCookingTime = React.useCallback(
        (event) =>
            handleInputChange(event, (cookingTime) =>
                dispatch(editRecipeActions.setCookingTime(cookingTime as number))
            ),
        [dispatch]
    );
    const setPortions = React.useCallback(
        (event) =>
            handleInputChange(event, (portions) =>
                dispatch(editRecipeActions.setPortions(portions as number))
            ),
        [dispatch]
    );

    return (
        <div className="recipeform-group recipeform_basic-info">
            <label className="recipeform-block">
                Reseptin nimi:
                <ValidatedInput
                    className={"recipeform__input"}
                    value={recipe.name}
                    onChange={setName}
                    validators={[noSpecialChars, notEmpty]}
                />
            </label>

            <label className="recipeform-block">
                Kategoria:
                <select
                    className="recipeform__input category-options"
                    value={recipe.category}
                    onChange={setCategory}
                >
                    <CategoryOptions />
                </select>
            </label>

            <label className="recipeform-block">
                Valmistusaika tunteina:
                <ValidatedInput
                    className="recipeform__input"
                    type="number"
                    value={recipe.cookingTime}
                    onChange={setCookingTime}
                    validators={[notEmpty, isNumeric, isPositive]}
                />
            </label>

            <label className="recipeform-block">
                Annokset:
                <ValidatedInput
                    className="recipeform__input"
                    type="number"
                    value={recipe.portions}
                    onChange={setPortions}
                    validators={[notEmpty, isNumeric, isPositive]}
                />
            </label>
        </div>
    );
};

function CategoryOptions(): any {
    return CATEGORIES.map((category) => {
        return <option key={category}>{category}</option>;
    });
}
