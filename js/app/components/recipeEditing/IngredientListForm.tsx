import * as React from "react";
import { MoveButtons, RemoveButton } from "./recipeEditing";
import { ValidatedInput } from "./validatedInput";
import { noSpecialChars } from "./FormValidators";
import { classNames } from "../../utils";
import { selectors, store, useAppDispatch, useAppSelector } from "../../state";
import { FIELD_TYPES, selectedFieldActions } from "../../state/editState/selectedField";
import { editRecipeActions } from "../../state/editState/currentRecipe";

export const IngredientListForm: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const ingredients = useAppSelector((state) => selectors.getEditableRecipe(state).ingredients);

    const addIngredient = React.useCallback(
        () => dispatch(editRecipeActions.addIngredient()),
        [dispatch]
    );
    const addSubtitle = React.useCallback(
        () => dispatch(editRecipeActions.addSubtitle()),
        [dispatch]
    );

    return (
        <div className="recipeform-group ingredientlist">
            <div className="recipeform-group__header">Ainekset:</div>
            <IngredientLabels />
            {ingredients.map((_, index) => (
                <IngredientListItem index={index} key={index} />
            ))}
            <div className="recipeform__ingredient-buttons">
                <button className="recipeform-add-button" type="button" onClick={addSubtitle}>
                    Lisää alaotsikko
                </button>
                <button className="recipeform-add-button" type="button" onClick={addIngredient}>
                    Lisää aines
                </button>
            </div>
        </div>
    );
};

const IngredientListItem: React.FC<{ index: number }> = ({ index }) => {
    const dispatch = useAppDispatch();

    const addIngredient = React.useCallback(
        () => dispatch(editRecipeActions.addIngredient()),
        [dispatch]
    );

    const ingredient = useAppSelector(
        (state) => selectors.getEditableRecipe(state).ingredients[index]
    );

    if (ingredient.subtitle !== undefined) {
        return (
            <IngredientFormRow index={index}>
                <IngredientInput
                    index={index}
                    field="subtitle"
                    onKeyDown={(event) => {
                        if (event.key === "Enter") {
                            addIngredient();
                        }
                    }}
                    placeholder="Alaotsikko"
                />
            </IngredientFormRow>
        );
    } else {
        return (
            <IngredientFormRow index={index}>
                <IngredientInput index={index} field="quantity" />
                <IngredientInput index={index} field="unit" />
                <IngredientInput
                    index={index}
                    field="name"
                    onKeyDown={(event) => {
                        if (event.key === "Enter") {
                            addIngredient();
                        }
                    }}
                />
            </IngredientFormRow>
        );
    }
};

export const IngredientFormRow: React.FC<
    React.PropsWithChildren<{
        index: number;
    }>
> = ({ index, children }) => {
    const dispatch = useAppDispatch();
    const isSelected = useAppSelector(
        (state) => selectors.getEditableField(state, FIELD_TYPES.INGREDIENTS) === index
    );
    const rowSelected = React.useCallback(() => {
        dispatch(selectedFieldActions.setEditableField({ index, field: FIELD_TYPES.INGREDIENTS }));
    }, [dispatch]);

    const moveIngredientUp = React.useCallback(
        (index) => {
            dispatch(editRecipeActions.moveIngredientUp(index));
            dispatch(
                selectedFieldActions.setEditableField({
                    index: Math.max(0, index - 1),
                    field: FIELD_TYPES.INGREDIENTS,
                })
            );
        },
        [dispatch]
    );
    const moveIngredientDown = React.useCallback(
        (index) => {
            dispatch(editRecipeActions.moveIngredientDown(index));
            const ingredientsLenght = selectors.getEditableRecipe(store.getState()).ingredients
                .length;
            dispatch(
                selectedFieldActions.setEditableField({
                    index: Math.min(ingredientsLenght - 1, index + 1),
                    field: FIELD_TYPES.INGREDIENTS,
                })
            );
        },
        [dispatch]
    );

    const removeIngredient = React.useCallback(
        (index) => dispatch(editRecipeActions.removeIngredient(index)),
        [dispatch]
    );

    const ingredientClasses = classNames({
        "ingredient-block": true,
        "ingredient-block--selected": isSelected,
    });
    return (
        <div className={ingredientClasses} key={index} tabIndex={-1} onFocus={rowSelected}>
            {isSelected && (
                <MoveButtons
                    index={index}
                    moveUpFunction={moveIngredientUp}
                    moveDownFunction={moveIngredientDown}
                />
            )}
            <div className="ingredient-row">{children}</div>
            {isSelected && <RemoveButton removeFunction={() => removeIngredient(index)} />}
        </div>
    );
};

const IngredientLabels: React.FC<{}> = () => (
    <div className="ingredient-label__block">
        <IngredientLabel>Määrä:</IngredientLabel>
        <IngredientLabel>Yksikkö:</IngredientLabel>
        <IngredientLabel>Nimi:</IngredientLabel>
    </div>
);

const IngredientLabel: React.FC<React.PropsWithChildren<{}>> = ({ children }) => (
    <div className="ingredient-label">{children}</div>
);

const IngredientInput: React.FC<{
    index: number;
    field: string;
    onKeyDown?: (event: React.KeyboardEvent<HTMLInputElement>) => void;
    placeholder?: string;
}> = ({ index, field, onKeyDown, placeholder }) => {
    const dispatch = useAppDispatch();

    const value = useAppSelector(
        (state) => selectors.getEditableRecipe(state).ingredients[index][field]
    );

    const onInputChange = React.useCallback(
        (event: React.ChangeEvent<HTMLInputElement>) => {
            dispatch(editRecipeActions.setIngredient({ index, field, value: event.target.value }));
        },
        [dispatch, index, field, value]
    );

    return (
        <div className="recipeform-block">
            <ValidatedInput
                className="recipeform__input"
                onChange={onInputChange}
                value={value}
                validators={[noSpecialChars]}
                onKeyDown={onKeyDown}
                placeholder={placeholder ?? ""}
            />
        </div>
    );
};
