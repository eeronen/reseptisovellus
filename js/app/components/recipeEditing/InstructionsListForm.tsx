import * as React from "react";
import { MoveButtons } from "./recipeEditing";
import { RemoveButton } from "./recipeEditing";
import { ValidatedInput } from "./validatedInput";
import { noSpecialChars, notEmpty } from "./FormValidators";
import { classNames } from "../../utils";
import { getEditableRecipe } from "../../state/selectors";
import { selectors, useAppDispatch, useAppSelector } from "../../state";
import { FIELD_TYPES, selectedFieldActions } from "../../state/editState/selectedField";
import { editRecipeActions } from "../../state/editState/currentRecipe";

export const InstructionListForm: React.FC<{}> = () => {
    const dispatch = useAppDispatch();

    const instructions = useAppSelector((state) => getEditableRecipe(state).instructions);

    const addInstruction = React.useCallback(
        () => dispatch(editRecipeActions.addInstruction()),
        [dispatch]
    );

    return (
        <div className="recipeform-group ingredientlist">
            <div className="recipeform-group__header">Työvaiheet:</div>
            {instructions.map((_, index) => (
                <InstructionListItem index={index} key={index} />
            ))}
            <button
                className="recipeform-add-button add-instruction"
                type="button"
                onClick={addInstruction}
            >
                Lisää työvaihe
            </button>
        </div>
    );
};

const InstructionListItem: React.FC<{ index: number }> = ({ index }) => {
    const dispatch = useAppDispatch();
    const instruction = useAppSelector(
        (state) => selectors.getEditableRecipe(state).instructions[index]
    );
    const isSelected = useAppSelector(
        (state) => selectors.getEditableField(state, FIELD_TYPES.INSTRUCTIONS) === index
    );

    const allInstructions = useAppSelector(
        (state) => selectors.getEditableRecipe(state).instructions
    );

    const removeInstruction = React.useCallback(
        () => dispatch(editRecipeActions.removeInstruction(index)),
        [dispatch]
    );
    const moveInstructionUp = React.useCallback(() => {
        dispatch(editRecipeActions.moveInstructionUp(index));
        dispatch(
            selectedFieldActions.setEditableField({
                index: Math.max(0, index - 1),
                field: FIELD_TYPES.INSTRUCTIONS,
            })
        );
    }, [dispatch]);
    const moveInstructionDown = React.useCallback(() => {
        dispatch(editRecipeActions.moveInstructionDown(index));
        const instructionsLenght = allInstructions.length;
        dispatch(
            selectedFieldActions.setEditableField({
                index: Math.min(instructionsLenght - 1, index + 1),
                field: FIELD_TYPES.INSTRUCTIONS,
            })
        );
    }, [dispatch, allInstructions]);

    const setSelectedInstruction = React.useCallback(
        () =>
            dispatch(
                selectedFieldActions.setEditableField({ index, field: FIELD_TYPES.INSTRUCTIONS })
            ),
        [dispatch]
    );

    const onInputChange = React.useCallback(
        (event: React.ChangeEvent<HTMLInputElement>) => {
            dispatch(editRecipeActions.setInstruction({ index, instruction: event.target.value }));
        },
        [dispatch, index]
    );

    const onInputKeydown = React.useCallback(
        (event: React.KeyboardEvent<HTMLInputElement>) => {
            if (event.key === "Enter") {
                dispatch(editRecipeActions.addInstruction());
            }
        },
        [dispatch]
    );

    const instructionClasses = classNames({
        "instruction-block": true,
        "instruction-block--selected": isSelected,
    });
    return (
        <div className={instructionClasses} tabIndex={-1} onFocus={setSelectedInstruction}>
            {isSelected && (
                <MoveButtons
                    index={index}
                    moveUpFunction={moveInstructionUp}
                    moveDownFunction={moveInstructionDown}
                />
            )}
            <div className="recipeform-instruction-group">
                <ValidatedInput
                    className="recipeform__input instruction-field"
                    onChange={onInputChange}
                    value={instruction}
                    onKeyDown={onInputKeydown}
                    validators={[noSpecialChars, notEmpty]}
                />
            </div>
            {isSelected && <RemoveButton removeFunction={removeInstruction} />}
        </div>
    );
};
