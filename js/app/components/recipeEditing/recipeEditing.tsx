import * as React from "react";
import { putRecipe, editRecipe, deleteRecipe } from "../../services/recipeservice";
import { BasicInfoForm } from "./BasicInfoForm";
import { IngredientListForm } from "./IngredientListForm";
import { InstructionListForm } from "./InstructionsListForm";
import { showModalDialog, showErrorDialog } from "../../commonComponents/modalDialog";
import { ValidateName } from "./FormValidators";

//@ts-ignore
import * as ArrowIcon from "../../../assets/icon-arrow.inline.svg";
import { store, selectors, useAppDispatch, useAppSelector } from "../../state";
import { resetEditableRecipe as resetEditableRecipeOperation } from "../../state/operations";
import { getEditableRecipe } from "../../state/selectors";
import { LoadingSpinner } from "../../commonComponents/loadSpinner";

export const RecipeEditing: React.FC<{ recipeName: string }> = ({ recipeName }) => {
    const dispatch = useAppDispatch();
    const resetEditableRecipe = React.useCallback(
        (name) => {
            dispatch(resetEditableRecipeOperation(name));
        },
        [dispatch]
    );

    const isRecipesLoaded = useAppSelector((state) => selectors.isRecipesLoaded(state));

    React.useEffect(() => {
        resetEditableRecipe(recipeName);
    }, [isRecipesLoaded, recipeName]);

    const submitRecipe = React.useCallback(() => {
        onSubmit(recipeName);
    }, [recipeName]);

    const resetRecipe = React.useCallback(() => resetEditableRecipe(recipeName), [recipeName]);

    const deleteRecipe = React.useCallback(() => onDeleteRecipe(recipeName), [recipeName]);

    if (isRecipesLoaded) {
        return (
            <div>
                <div className="recipeform-header">
                    {recipeName === "uusi" ? "Lisää Resepti" : "Muokkaa reseptiä"}
                </div>
                <div className="recipeform">
                    <BasicInfoForm />
                    <IngredientListForm />
                    <InstructionListForm />
                </div>
                <button className="submitbutton submit" onClick={submitRecipe}>
                    {recipeName === "uusi" ? "Lisää" : "Tallenna"}
                </button>
                <button className="submitbutton reset" onClick={resetRecipe}>
                    Peru muutokset
                </button>
                {recipeName !== "uusi" && (
                    <button className="submitbutton delete" onClick={deleteRecipe}>
                        Poista Resepti
                    </button>
                )}
            </div>
        );
    } else {
        return <LoadingSpinner />;
    }
};

export const MoveButtons: React.FC<{
    index: number;
    moveUpFunction: (index: number) => void;
    moveDownFunction: (index: number) => void;
}> = ({ index, moveDownFunction, moveUpFunction }) => {
    const onMoveUpClick = React.useCallback(() => moveUpFunction(index), [moveUpFunction, index]);
    const onMoveDownClick = React.useCallback(
        () => moveDownFunction(index),
        [moveDownFunction, index]
    );
    return (
        <div className="movebuttons-container">
            <div className="icon movebutton moveup" onClick={onMoveUpClick}>
                <ArrowIcon />
            </div>
            <div className="icon movebutton movedown" onClick={onMoveDownClick}>
                <ArrowIcon />
            </div>
        </div>
    );
};

export const RemoveButton: React.FC<{ removeFunction: () => void }> = (props) => (
    <div className="icon-container icon-remove" onClick={props.removeFunction}>
        X
    </div>
);

const validateRecipe = async (recipeName: string) => {
    if (document.getElementsByClassName("invalid").length !== 0) {
        return false;
    } else if (!(await ValidateName(recipeName, getEditableRecipe(store.getState()).name))) {
        return false;
    }
    return true;
};

const onSubmit = async (recipeName: string) => {
    const isFormValid = await validateRecipe(recipeName);

    if (!isFormValid) {
        showErrorDialog({
            header: "Virhe!",
            message: "Reseptissä on virheitä. tarkista resepti",
        });
    } else if (recipeName === "uusi") {
        putRecipe(getEditableRecipe(store.getState())).then(() => {
            showModalDialog({
                header: "Valmis",
                message: "Resepti lisätty onnistuneesti.",
                primaryAction: redirectToFrontpage,
            });
        });
    } else {
        editRecipe(getEditableRecipe(store.getState())).then(() => {
            showModalDialog({
                header: "Valmis",
                message: "Resepti päivitetty onnistuneesti.",
                primaryAction: redirectToFrontpage,
            });
        });
    }
};

const onDeleteRecipe = (recipeName: string) => {
    const confirmation = confirm("Haluatko varmasti poistaa tämän reseptin?");
    if (confirmation) {
        deleteRecipe(recipeName).then(() => {
            showModalDialog({
                header: "Valmis",
                message: "Resepti poistettu onnistuneesti.",
                primaryAction: redirectToFrontpage,
            });
        });
    }
};

const redirectToFrontpage = () => window.location.replace(window.location.origin);
