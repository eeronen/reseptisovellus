import "mocha";
import * as React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";

import { BasicInfoForm, BasicInfoFormProps } from "../BasicInfoForm";

const basicInfoFormProps: BasicInfoFormProps = {
    name: "",
    category: "",
    cookingTime: 0,
    portions: 0,
    setCategory: _ => {},
    setCookingTime: _ => {},
    setName: _ => {},
    setPortions: _ => {},
};

describe("basicInfoForm", () => {
    it("Should contain all fields for empty recipe", () => {
        const wrapper = shallow(<BasicInfoForm {...basicInfoFormProps} />);
        expect(wrapper.find(".recipeform__input")).to.have.lengthOf(4);
        expect(wrapper.text()).to.contain("Reseptin nimi");
        expect(wrapper.text()).to.contain("Kategoria");
        expect(wrapper.text()).to.contain("Valmistusaika");
        expect(wrapper.text()).to.contain("Annokset");
    });
});
