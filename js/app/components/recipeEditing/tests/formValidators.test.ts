import "mocha";
import { expect } from "chai";

import { noSpecialChars, notEmpty, isNumeric, isPositive } from "../FormValidators";

describe("formValidators", () => {
    it("should disallow quote characters", () => {
        expect(noSpecialChars('fo"o').isValid).to.be.false;
        expect(noSpecialChars("fo'o").isValid).to.be.false;
        expect(noSpecialChars("fo´o").isValid).to.be.false;
        expect(noSpecialChars("fo`o").isValid).to.be.false;
    });

    it("should allow some special characters", () => {
        expect(noSpecialChars("foo-bar").isValid).to.be.true;
        expect(noSpecialChars("foo_bar").isValid).to.be.true;
        expect(noSpecialChars("foo~bar").isValid).to.be.true;
        expect(noSpecialChars("foo*bar").isValid).to.be.true;
    });

    it("should disallow empty strings", () => {
        expect(notEmpty("").isValid).to.be.false;
    });

    it("should allow non-empty strings", () => {
        expect(notEmpty("foobar").isValid).to.be.true;
    });

    it("should allow numerics", () => {
        expect(isNumeric("1").isValid).to.be.true;
        expect(isNumeric("0").isValid).to.be.true;
        expect(isNumeric("-1").isValid).to.be.true;
    });

    it("should disallow non numerical values", () => {
        expect(isNumeric("foobar").isValid).to.be.false;
        expect(isNumeric("12foo").isValid).to.be.false;
        expect(isNumeric("-foo12").isValid).to.be.false;
    });

    it("should allow positive numbers", () => {
        expect(isPositive(1).isValid).to.be.true;
        expect(isPositive(100000000000).isValid).to.be.true;
        expect(isPositive(0).isValid).to.be.true;
    });

    it("should disallow negative numbers", () => {
        expect(isPositive(-1).isValid).to.be.false;
        expect(isPositive(-1234567890).isValid).to.be.false;
        expect(isPositive(-0.0000001).isValid).to.be.false;
    });

    it("should contain an error message for each validator", () => {
        expect(noSpecialChars("foo`bar").message).to.not.be.empty;
        expect(notEmpty("").message).to.not.be.empty;
        expect(isNumeric("foobar").message).to.not.be.empty;
        expect(isPositive(-1).message).to.not.be.empty;
    });
});
