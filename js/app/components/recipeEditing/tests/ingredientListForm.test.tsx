import "mocha";
import * as React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";
import * as sinon from "sinon";

import {
    IngredientListForm,
    IngredientFormRow,
    IngredientListFormProps,
} from "../IngredientListForm";
import { MoveButtons, RemoveButton } from "../recipeEditing";

const defaultProps: IngredientListFormProps = {
    ingredients: [],
    editableIndex: undefined,
    addIngredient: () => {},
    addSubtitle: () => {},
    setEditableIndex: _ => {},
    setIngredient: (index, field, value) => {},
    moveIngredientDown: _ => {},
    moveIngredientUp: _ => {},
    removeIngredient: () => {},
};

describe("ingredientListForm", () => {
    it("Should contain title", () => {
        const wrapper = shallow(<IngredientListForm {...defaultProps} />);
        expect(wrapper.text()).to.contain("Ainekset");
    });

    it("should contain adding buttons", () => {
        const wrapper = shallow(<IngredientListForm {...defaultProps} />);
        expect(wrapper.find("button")).to.have.lengthOf(2);
    });

    it("should add new subtitle", () => {
        const addSubtitle = sinon.fake();
        const wrapper = shallow(<IngredientListForm {...{ ...defaultProps, addSubtitle }} />);
        wrapper
            .find("button")
            .first()
            .simulate("click");
        expect(addSubtitle.called).to.be.true;
    });

    it("should add new ingredient", () => {
        const addIngredient = sinon.fake();
        const wrapper = shallow(<IngredientListForm {...{ ...defaultProps, addIngredient }} />);
        wrapper
            .find("button")
            .last()
            .simulate("click");
        expect(addIngredient.called).to.be.true;
    });

    it("Should contain move and remove row buttons", () => {
        const wrapper = shallow(
            <IngredientFormRow
                isSelected={true}
                index={1}
                rowSelected={() => {}}
                moveIngredientUp={() => {}}
                removeIngredient={() => {}}
                moveIngredientDown={() => {}}
            />
        );

        expect(wrapper.find(MoveButtons)).to.have.lengthOf(1);
        expect(wrapper.find(RemoveButton)).to.have.lengthOf(1);
    });

    it("Should not contain move and remove row buttons", () => {
        const wrapper = shallow(
            <IngredientFormRow
                isSelected={false}
                index={1}
                rowSelected={() => {}}
                moveIngredientUp={() => {}}
                moveIngredientDown={() => {}}
                removeIngredient={() => {}}
            />
        );

        expect(wrapper.find(MoveButtons)).to.have.lengthOf(0);
        expect(wrapper.find(RemoveButton)).to.have.lengthOf(0);
    });
});
