import "mocha";
import * as React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";
import * as sinon from "sinon";

import { InstructionListForm, InstructionListFormProps } from "../InstructionsListForm";

const defaultProps: InstructionListFormProps = {
    instructions: [],
    addInstruction: () => {},
    removeInstruction: () => {},
    moveInstructionDown: _ => {},
    moveInstructionUp: _ => {},
    setInstruction: () => {},
    editableIndex: undefined,
    setSelectedInstruction: _ => {},
};

describe("instructionsListForm", () => {
    it("Should contain title", () => {
        const wrapper = shallow(<InstructionListForm {...defaultProps} />);
        expect(wrapper.text()).to.contain("Työvaiheet");
    });

    it("should contain adding buttons", () => {
        const wrapper = shallow(<InstructionListForm {...defaultProps} />);

        expect(wrapper.find("button")).to.have.lengthOf(1);
    });
    it("should add new instruction", () => {
        const addInstruction = sinon.fake();
        const wrapper = shallow(
            <InstructionListForm {...{ ...defaultProps, addInstruction: addInstruction }} />
        );
        wrapper.find("button").simulate("click");
        expect(addInstruction.called).to.be.true;
    });
});
