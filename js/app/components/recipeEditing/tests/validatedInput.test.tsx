import "mocha";
import * as React from "react";
import { expect } from "chai";
import { mount } from "enzyme";

import { ValidatedInput, getValidationErrors } from "../validatedInput";

describe("validatedInput", () => {
    it("Should not display errors when valid", () => {
        const validator = (_: any) => ({ isValid: true, message: "" });
        const wrapper = mount(
            <ValidatedInput
                className="inputClass"
                value="foo"
                onChange={() => undefined}
                validators={[validator]}
            />
        );
        expect(wrapper.find(".valid")).to.have.lengthOf(1);
        expect(wrapper.find(".invalid")).to.have.lengthOf(0);
    });

    it("Should display errors", () => {
        const validator = (_: any) => ({ isValid: false, message: "errormessage" });
        const wrapper = mount(
            <ValidatedInput
                className="inputClass"
                value="foo"
                onChange={() => undefined}
                validators={[validator]}
            />
        );
        expect(wrapper.find(".invalid")).to.have.lengthOf(1);
        expect(wrapper.find(".valid")).to.have.lengthOf(0);
    });

    it("Should concatenate errors", () => {
        const validators = [
            (_: any) => ({ isValid: false, message: "errormessage1" }),
            (_: any) => ({ isValid: true, message: "" }),
            (_: any) => ({ isValid: false, message: "errormessage2" }),
        ];
        const errors = getValidationErrors(validators, "");
        expect(errors).to.equal("errormessage1. errormessage2");
    });
});
