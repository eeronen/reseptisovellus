import * as React from "react";
import { PortionControl } from "./portions";

import { selectors, useAppDispatch, useAppSelector } from "../../state";
import { LoadingSpinner } from "../../commonComponents/loadSpinner";
import { addToHistory } from "../../utils/historyUtils";
import { viewStateAction } from "../../state/viewState";
import { IngredientList } from "./ingredients";
import { InstructionList } from "./instructions";
import { RecipeViewHeader } from "./recipeViewHeader";

export const RecipeView: React.FC<{ recipeName: string }> = ({ recipeName }) => {
    const dispatch = useAppDispatch();

    const resetPortions = React.useCallback(() => {
        dispatch(viewStateAction.setPortionsMultiplier(1));
    }, [dispatch]);
    const resetUnits = React.useCallback(() => {
        dispatch(viewStateAction.resetUnits());
    }, [dispatch]);

    const recipesLoaded = useAppSelector((state) => selectors.isRecipesLoaded(state));

    React.useEffect(() => {
        document.title = recipeName;
        resetPortions();
        addToHistory(recipeName);
        resetUnits();
    }, [recipeName]);

    if (!recipesLoaded) {
        return <LoadingSpinner />;
    } else {
        return (
            <div className="recipe-details__content">
                <RecipeViewHeader recipeName={recipeName} />
                <PortionControl recipeName={recipeName} />
                <div className="recipe-details__instructions">
                    <IngredientList recipeName={recipeName} />
                    <InstructionList recipeName={recipeName} />
                </div>
            </div>
        );
    }
};
