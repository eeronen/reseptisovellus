import * as React from "react";
import { IngredientList } from "./ingredients";
import { InstructionList } from "./instructions";
import { modalRoot } from "../../commonComponents/modalDialog";

export const showEmbeddedRecipe = (recipeName: string) => {
    modalRoot.render(<EmbeddedRecipe recipeName={recipeName} />);
};

export const EmbeddedRecipe: React.FC<{ recipeName: string }> = ({ recipeName }) => (
    <div className="modal-dialog__background">
        <div className="modal-dialog">
            <ul>
                <IngredientList recipeName={recipeName} />
            </ul>
            <ul>
                <InstructionList recipeName={recipeName} />
            </ul>
        </div>
    </div>
);
