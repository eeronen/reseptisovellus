import * as React from "react";
import { RoundTo2Decimals } from "../../utils/maths";
import { Ingredient } from "../../recipe";
import { LoadingSpinner } from "../../commonComponents/loadSpinner";
import { selectors, useAppDispatch, useAppSelector } from "../../state";
import { setPortionsMultiplierWithPortionsNumber } from "../../state/operations";
import { getAsOtherUnit, getPossibleUnits, UnitName } from "../../utils/unitChanger";
import { classNames } from "../../utils";
import { viewStateAction } from "../../state/viewState";

export const IngredientList: React.FC<{ recipeName: string }> = ({ recipeName }) => {
    const recipe = useAppSelector((state) => selectors.getRecipeByName(state, recipeName));

    if (recipe !== undefined && recipe.ingredients.length !== 0) {
        return (
            <ul className="ingredient-list">
                {recipe.ingredients.map((_, index) => (
                    <IngredientListItem key={index} index={index} recipeName={recipeName} />
                ))}
            </ul>
        );
    } else {
        return <LoadingSpinner />;
    }
};

const IngredientListItem: React.FC<{
    index: number;
    recipeName: string;
}> = ({ recipeName, index }) => {
    const [isInEditMode, setEditMode] = React.useState(false);

    const ingredient = useAppSelector(
        (state) => selectors.getRecipeByName(state, recipeName)?.ingredients[index]
    );

    if (ingredient === undefined) {
        return null;
    }

    if (ingredient.subtitle) {
        return <h1 className="ingredient-list__subtitle">{ingredient.subtitle}</h1>;
    }

    if (isInEditMode) {
        return (
            <EditModeIngredient
                index={index}
                ingredient={ingredient}
                recipeName={recipeName}
                onEditingEnded={() => setEditMode(false)}
            />
        );
    } else {
        return (
            <ViewModeIngredient
                index={index}
                onEditingStarted={() => setEditMode(true)}
                ingredient={ingredient}
            />
        );
    }
};

const ViewModeIngredient: React.FC<{
    index: number;
    onEditingStarted: () => void;
    ingredient: Ingredient;
}> = ({ index, onEditingStarted, ingredient }) => {
    const displayUnit = useAppSelector(
        (state) => selectors.getSelectedUnits(state)[index] ?? ingredient.unit
    );
    const portionMultiplier = useAppSelector((state) => selectors.getPortionsMultiplier(state));

    const displayQuantity = getAsOtherUnit(ingredient.unit, ingredient.quantity, displayUnit);

    return (
        <li className="ingredient-list__ingredient-container">
            <div className="ingredient-list__ingredient-quantity" onClick={onEditingStarted}>
                {ingredient.quantity !== 0 && RoundTo2Decimals(displayQuantity * portionMultiplier)}{" "}
                {displayUnit}
            </div>
            <div className="ingredient-list__ingredient-name">{getWholeName(ingredient)}</div>
        </li>
    );
};

const EditModeIngredient: React.FC<{
    recipeName: string;
    index: number;
    onEditingEnded: () => void;
    ingredient: Ingredient;
}> = ({ recipeName, index, onEditingEnded, ingredient }) => {
    const inputRef = React.useRef<HTMLInputElement>(null);

    const dispatch = useAppDispatch();

    const displayUnit = useAppSelector(
        (state) => selectors.getSelectedUnits(state)[index] ?? ingredient.unit
    );
    const originalPortions = useAppSelector(
        (state) => selectors.getRecipeByName(state, recipeName)?.portions ?? 0
    );
    const portionMultiplier = useAppSelector((state) => selectors.getPortionsMultiplier(state));

    const setPortions = React.useCallback(
        (portions) => {
            dispatch(setPortionsMultiplierWithPortionsNumber(recipeName, portions));
        },
        [dispatch, recipeName]
    );

    const selectUnit = React.useCallback(
        (unit) => {
            dispatch(viewStateAction.setUnit({ index, unit }));
        },
        [dispatch, index]
    );

    if (ingredient === undefined) {
        return null;
    }

    const displayQuantity = getAsOtherUnit(ingredient.unit, ingredient.quantity, displayUnit);

    return (
        <li className="ingredient-list__ingredient-container" onBlur={onEditingEnded}>
            <div className="ingredient-list__ingredient-input">
                <input
                    className="ingredient-list__ingredient-quantity ingredient-list__ingredient-quantity--input"
                    type="number"
                    min={0}
                    step="any"
                    autoFocus={true}
                    onFocus={(e) => e.target.select()}
                    onChange={(event) => {
                        const newQuantity = getAsOtherUnit(
                            displayUnit,
                            Number(event.target.value),
                            ingredient.unit
                        );

                        const originalQuantity = ingredient.quantity ?? 1;
                        const portionsOnRecipe = originalPortions || 1;

                        const newPortions = (newQuantity / originalQuantity) * portionsOnRecipe;
                        setPortions(newPortions);
                    }}
                    onKeyPress={(event) => {
                        if (event.key === "Enter") {
                            onEditingEnded();
                        }
                    }}
                    defaultValue={RoundTo2Decimals(displayQuantity * portionMultiplier)}
                    ref={inputRef}
                />
                <UnitChanger
                    currentUnit={displayUnit}
                    selectUnit={(newUnit) => {
                        if (inputRef.current !== undefined) {
                            (inputRef.current as any).value =
                                newUnit === null
                                    ? ingredient.quantity
                                    : RoundTo2Decimals(
                                          getAsOtherUnit(
                                              ingredient.unit,
                                              ingredient.quantity,
                                              newUnit
                                          )
                                      ) * portionMultiplier;
                        }
                        selectUnit(newUnit);
                    }}
                />
            </div>
            <div className="ingredient-list__ingredient-name">{getWholeName(ingredient)}</div>
        </li>
    );
};

const UnitChanger: React.FC<{
    currentUnit?: string;
    selectUnit: (unit: UnitName) => void;
}> = (props) => {
    const availableUnits = getPossibleUnits(props.currentUnit);
    if (availableUnits.length > 0) {
        return (
            <div className="unit-changer__list">
                {availableUnits.map((unitName) => {
                    const classes = classNames({
                        "unit-changer__list-item": true,
                        "unit-changer__list-item--selected": unitName === props.currentUnit,
                    });
                    return (
                        <div
                            className={classes}
                            key={unitName}
                            onMouseDown={() => props.selectUnit(unitName)}
                        >
                            {unitName}
                        </div>
                    );
                })}
            </div>
        );
    } else {
        return null;
    }
};

const getWholeName = (ingredient: Ingredient) =>
    [ingredient.prefix, ingredient.name, ingredient.postfix]
        .filter((str) => str !== undefined)
        .join(" ");
