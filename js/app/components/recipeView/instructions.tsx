import * as React from "react";
import { selectors, useAppSelector } from "../../state";
import { makeInstructionNumberScaler } from "../../utils/scaleUtils";

export const InstructionList: React.FC<{ recipeName: string }> = ({ recipeName }) => {
    const portionMultiplier = useAppSelector((state) => selectors.getPortionsMultiplier(state));
    const recipe = useAppSelector((state) => selectors.getRecipeByName(state, recipeName));
    const instructions = React.useMemo(
        () => recipe?.instructions.map(makeInstructionNumberScaler(portionMultiplier)),
        [recipe]
    );

    if (instructions) {
        return (
            <ol className="instructions-list__list">
                {instructions.map((instruction, i) => {
                    return (
                        <li className="instructions-list__instruction" key={i}>
                            {instruction}
                        </li>
                    );
                })}
            </ol>
        );
    } else {
        return null;
    }
};
