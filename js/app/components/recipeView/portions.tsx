import * as React from "react";
import { RoundToOneDecimal } from "../../utils/maths";
import { selectors, useAppDispatch, useAppSelector } from "../../state";
import { NEW_RECIPE } from "../../recipe";
import { setPortionsMultiplierWithPortionsNumber } from "../../state/operations";

export const PortionControl: React.FC<{ recipeName: string }> = ({ recipeName }) => {
    const dispatch = useAppDispatch();

    const portions = useAppSelector((state) => {
        const originalPortions = (selectors.getRecipeByName(state, recipeName) || NEW_RECIPE)
            .portions;
        const portionMultiplier = selectors.getPortionsMultiplier(state);
        return originalPortions * portionMultiplier;
    });

    const handlePlusClick = React.useCallback(() => {
        let newPortions = portions;
        if (Number.isInteger(portions)) {
            newPortions++;
        } else {
            newPortions = Math.ceil(newPortions);
        }

        dispatch(setPortionsMultiplierWithPortionsNumber(recipeName, newPortions));
    }, [dispatch, portions]);

    const handleMinusClick = React.useCallback(() => {
        let newPortions = portions;
        if (Number.isInteger(portions)) {
            newPortions--;
        } else {
            newPortions = Math.floor(newPortions);
        }

        dispatch(setPortionsMultiplierWithPortionsNumber(recipeName, newPortions));
    }, [dispatch, portions]);

    return (
        <div className="portion-control__container">
            <label className="portion-control__portions-label">Annokset</label>
            <div onClick={handleMinusClick} className="portion-control__change-portions minus">
                -
            </div>
            <div className="portion-control__change-portions portion-control__portion-number">
                {RoundToOneDecimal(portions)}
            </div>
            <div onClick={handlePlusClick} className="portion-control__change-portions plus">
                +
            </div>
        </div>
    );
};
