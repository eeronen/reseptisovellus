import * as React from "react";

import { selectors, useAppDispatch, useAppSelector } from "../../state";
import { FavoriteControl } from "./favoriteControl";
//@ts-ignore
import * as Bookmark from "../../../assets/icon-bookmark.inline.svg";
//@ts-ignore
import * as UncheckedBookmark from "../../../assets/icon-bookmark-unchecked.inline.svg";
//@ts-ignore
import * as EditIcon from "../../../assets/icon-edit.inline.svg";
//@ts-ignore
import * as PhotoIcon from "../../../assets/icon-plus.inline.svg";

import { environment } from "../../../environments/environment";
import { Modal, showErrorDialog } from "../../commonComponents/modalDialog";
import { Link } from "react-router-dom";
import { appActions } from "../../state/appState";

export const RecipeViewHeader: React.FC<{ recipeName: string }> = ({ recipeName }) => {
    const dispatch = useAppDispatch();

    const addTab = React.useCallback(() => {
        dispatch(appActions.addTab(recipeName));
    }, [dispatch, recipeName]);
    const removeTab = React.useCallback(() => {
        dispatch(appActions.removeTab(recipeName));
    }, [dispatch, recipeName]);

    const isTabbed = useAppSelector((state) => selectors.getTabs(state).includes(recipeName));
    const picture = useAppSelector((state) => selectors.getPictureForRecipe(state, recipeName));
    const isLoggedIn = useAppSelector((state) => selectors.isLoggedIn(state));

    return (
        <div className="recipe-details__header">
            {picture && <RecipePicture source={picture} />}
            <div className="recipe-details__title-block">
                <div className="recipe-details__name">{recipeName}</div>
                <div className="recipe-details__buttons">
                    <FavoriteControl recipeName={recipeName} />
                    <div
                        className="icon recipe-details__icon"
                        onClick={isTabbed ? removeTab : addTab}
                    >
                        {isTabbed ? <Bookmark /> : <UncheckedBookmark />}
                    </div>
                    {isLoggedIn && <EditRecipeIcon recipeName={recipeName} />}
                    {isLoggedIn && <PictureUploadIcon recipeName={recipeName} />}
                </div>
            </div>
        </div>
    );
};

const EditRecipeIcon: React.FC<{ recipeName: string }> = (props) => {
    return (
        <Link to={"/reseptin-muokkaus/" + props.recipeName}>
            <div className="icon recipe-details__icon">
                <EditIcon />
            </div>
        </Link>
    );
};

const PictureUploadIcon: React.FC<{ recipeName: string }> = (props) => (
    <>
        <div
            className="icon recipe-details__icon"
            onClick={() => {
                const uploadInput = document.querySelector("#pictureUpload") as HTMLElement;
                if (uploadInput !== null) {
                    uploadInput.click();
                }
            }}
        >
            <PhotoIcon />
        </div>
        <PictureUploadInput {...props} />
    </>
);

const PictureUploadInput: React.FC<{ recipeName: string }> = (props) => {
    const [progress, setProgress] = React.useState(0);
    return (
        <>
            <input
                className="recipe-details__picture-upload--input"
                id="pictureUpload"
                type="file"
                name="recipeImage"
                accept="image/*"
                onChange={(event) => {
                    if (event.currentTarget.files === null) {
                        return;
                    }
                    const blob = event.currentTarget.files[0];
                    const formData = new FormData();
                    formData.append("recipeImage", blob);

                    const xmlhttp = new XMLHttpRequest();
                    xmlhttp.onerror = () => {
                        setProgress(0);
                        showErrorDialog({
                            header: "Picture upload error",
                            message: xmlhttp.responseText,
                        });
                    };
                    xmlhttp.onload = () => {
                        setProgress(0);
                        if (xmlhttp.status !== 200) {
                            showErrorDialog({
                                header: "Picture upload error",
                                message: xmlhttp.responseText,
                            });
                        } else {
                            window.location.reload();
                        }
                    };
                    xmlhttp.open(
                        "POST",
                        environment.apiUrl + "/uploads/upload_photo.php?name=" + props.recipeName,
                        true
                    );
                    xmlhttp.upload.onprogress = (e) => {
                        if (e.lengthComputable) {
                            setProgress((e.loaded / e.total) * 100);
                        }
                    };
                    xmlhttp.send(formData);
                }}
            />
            {progress > 0 && (
                <Modal header="Ladataan kuvaa">
                    <div className="upload-dialog__progress-bar">
                        <div
                            className="upload-dialog__progress-bar--filler"
                            style={{ width: `${progress}%` }}
                        />
                    </div>
                </Modal>
            )}
        </>
    );
};

const RecipePicture: React.FC<{ source: string }> = (props) => (
    <img src={`/api/uploads/${props.source}`} className="recipe-details__picture" />
);
