import "mocha";
import * as React from "react";
import { expect } from "chai";
import { render, mount } from "enzyme";

import { Ingredient } from "../../../recipe";
import { IngredientList } from "../ingredients";

const ingredients: Ingredient[] = [
    {
        subtitle: "Subtitle1",
    },
    {
        name: "aaa",
        unit: "u1",
        quantity: 1,
    },
    {
        name: "bbb",
        quantity: 1.23456,
    },
    {
        name: "ccc",
        quantity: 0,
    },
];
const wrapper = mount(
    <IngredientList
        ingredients={ingredients}
        setEditableIndex={() => {}}
        setPortions={() => {}}
        portionMultiplier={1}
        isInEditMode={false}
        editableIndex={undefined}
        originalPortions={1}
        recipeName="test"
        selectedUnits={[null, null, null, null]}
        selectUnit={(_, __) => {}}
    />
);

describe("ingredients", () => {
    it("Should render list of ingredients", () => {
        expect(wrapper.find("li")).to.have.lengthOf(3);
    });

    it("Should render subtitle", () => {
        expect(wrapper.find(".ingredient-list__subtitle")).to.have.lengthOf(1);
    });

    it("Should round to 2 decimals", () => {
        const roundedWrapper = render(
            <IngredientList
                ingredients={[ingredients[2]]}
                setEditableIndex={() => {}}
                setPortions={() => {}}
                portionMultiplier={1}
                isInEditMode={false}
                editableIndex={undefined}
                originalPortions={1}
                recipeName="test"
                selectedUnits={[null, null, null, null]}
                selectUnit={(_, __) => {}}
            />
        );
        expect(roundedWrapper.find(".ingredient-list__ingredient-quantity").text()).to.equal(
            "1.23 "
        );
    });

    it("Should omit quantity of 0", () => {
        const omittedWrapper = render(
            <IngredientList
                ingredients={[ingredients[3]]}
                setEditableIndex={() => {}}
                setPortions={() => {}}
                portionMultiplier={1}
                isInEditMode={false}
                editableIndex={undefined}
                originalPortions={1}
                recipeName="test"
                selectedUnits={[null, null, null, null]}
                selectUnit={(_, __) => {}}
            />
        );
        expect(omittedWrapper.find(".ingredient-list__ingredient-quantity").text()).to.equal(" ");
    });
});
