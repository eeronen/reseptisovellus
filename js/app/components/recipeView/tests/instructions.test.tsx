import "mocha";
import * as React from "react";
import { expect } from "chai";
import { mount } from "enzyme";

import { InstructionList } from "../instructions";

describe("instructions", () => {
    it("Should render list of instructions", () => {
        const instructions = ["instruction1", "instruction2", "instruction3"];
        const wrapper = mount(<InstructionList recipeName="foobar" instructions={instructions} />);

        expect(wrapper.find("li")).to.have.lengthOf(3);
    });
});
