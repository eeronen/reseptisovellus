import "mocha";
import * as React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";

import { PortionControl } from "../portions";

describe("portions", () => {
    it("Portions should be rendered", () => {
        const wrapper = shallow(<PortionControl portions={1} updatePortions={() => {}} />);
        expect(wrapper.find(".portion-control__container")).to.have.lengthOf(1);
    });

    it("Should increase portions", () => {
        var portions = 1;
        const updatePortions = (newPortions: number) => {
            portions = newPortions;
        };

        const wrapper = shallow(
            <PortionControl portions={portions} updatePortions={updatePortions} />
        );
        wrapper.find(".plus").simulate("click");
        expect(portions).to.equal(2);
    });

    it("Should decrease portions", () => {
        var portions = 2;
        const updatePortions = (newPortions: number) => {
            portions = newPortions;
        };

        const wrapper = shallow(
            <PortionControl portions={portions} updatePortions={updatePortions} />
        );
        wrapper.find(".minus").simulate("click");
        expect(portions).to.equal(1);
    });

    it("Should round portions to one decimal", () => {
        var portions = 1.123456789;
        const updatePortions = (newPortions: number) => {
            portions = newPortions;
        };

        const wrapper = shallow(
            <PortionControl portions={portions} updatePortions={updatePortions} />
        );
        expect(wrapper.find(".portion-control__portion-number").text()).to.equal("1.1");
    });

    it("Should increase float to next integer", () => {
        var portions = 1.2;
        const updatePortions = (newPortions: number) => {
            portions = newPortions;
        };

        const wrapper = shallow(
            <PortionControl portions={portions} updatePortions={updatePortions} />
        );
        wrapper.find(".plus").simulate("click");
        expect(portions).to.equal(2);
    });

    it("Should decrease float to next integer", () => {
        var portions = 1.2;
        const updatePortions = (newPortions: number) => {
            portions = newPortions;
        };

        const wrapper = shallow(
            <PortionControl portions={portions} updatePortions={updatePortions} />
        );
        wrapper.find(".minus").simulate("click");
        expect(portions).to.equal(1);
    });
});
