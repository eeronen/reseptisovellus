/**
 * Created by Eero Saarinen (eero.saarinen@hotmail.fi)
 * Created under Creative Commons licence
 */

export interface Recipe {
    name: string;
    category: string;
    portions: number;
    cookingTime: number;
    ingredients: Ingredient[];
    instructions: string[];
    description: string;
    picture: string | null;
}

export const NEW_RECIPE: Recipe = {
    name: "",
    category: "Muut",
    portions: 0,
    cookingTime: 0,
    ingredients: [],
    instructions: [],
    description: "",
    picture: null,
};

export interface Ingredient {
    name?: string;
    quantity?: number;
    unit?: string;
    subtitle?: string;
    prefix?: string;
    postfix?: string;
}

export const NEW_SUBTITLE: Ingredient = {
    subtitle: "",
};

export const NEW_INGREDIENT: Ingredient = {
    name: "",
    quantity: 0,
    unit: "",
};

export function newSubtitle() {
    return { ...NEW_SUBTITLE };
}

export function newIngredient() {
    return { ...NEW_INGREDIENT };
}
