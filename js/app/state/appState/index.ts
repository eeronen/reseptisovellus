import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState = {
    tabs: [] as string[],
    searchValue: "",
    isLoggedIn: false,
    isSearchExpanded: false,
    isMenuExpanded: false,
};

export const appStateSlice = createSlice({
    name: "app",
    initialState,
    reducers: {
        addTab: (state, action: PayloadAction<string>) => {
            state.tabs.push(action.payload);
        },
        removeTab: (state, action: PayloadAction<string>) => {
            const removeIndex = state.tabs.findIndex((tabName) => tabName === action.payload);
            state.tabs.splice(removeIndex, 1);
        },
        setSearchValue: (state, action: PayloadAction<string>) => {
            state.searchValue = action.payload;
        },
        setLoggedIn: (state, action: PayloadAction<boolean>) => {
            state.isLoggedIn = action.payload;
        },
        setSearchExpanded: (state, action: PayloadAction<boolean>) => {
            state.isSearchExpanded = action.payload;
        },
        setMenuExpanded: (state, action: PayloadAction<boolean>) => {
            state.isMenuExpanded = action.payload;
        },
    },
});

export const appReducer = appStateSlice.reducer;
export const appActions = appStateSlice.actions;
