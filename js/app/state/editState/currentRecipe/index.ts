import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { Recipe, newIngredient, newSubtitle } from "../../../recipe";

const initialState: Recipe = {
    name: "",
    category: "",
    cookingTime: 0,
    description: "",
    ingredients: [],
    instructions: [],
    picture: null,
    portions: 0,
};

const editRecipeSlice = createSlice({
    initialState,
    name: "editRecipe",
    reducers: {
        initializeEditableRecipe: (state, action: PayloadAction<Recipe>) => {
            state.name = action.payload.name;
            state.category = action.payload.category;
            state.cookingTime = action.payload.cookingTime;
            state.description = action.payload.description;
            state.ingredients = action.payload.ingredients;
            state.instructions = action.payload.instructions;
            state.picture = action.payload.picture;
            state.portions = action.payload.portions;
        },
        setRecipeName: (state, action: PayloadAction<string>) => {
            state.name = action.payload;
        },
        setRecipePicture: (state, action: PayloadAction<string>) => {
            state.picture = action.payload;
        },
        setCategory: (state, action: PayloadAction<string>) => {
            state.category = action.payload;
        },
        setPortions: (state, action: PayloadAction<number>) => {
            state.portions = action.payload;
        },
        setCookingTime: (state, action: PayloadAction<number>) => {
            state.cookingTime = action.payload;
        },
        setDescription: (state, action: PayloadAction<string>) => {
            state.description = action.payload;
        },
        setInstruction: (state, action: PayloadAction<{ index: number; instruction: string }>) => {
            state.instructions[action.payload.index] = action.payload.instruction;
        },
        addInstruction: (state) => {
            state.instructions.push("");
        },
        removeInstruction: (state, action: PayloadAction<number>) => {
            state.instructions.splice(action.payload, 1);
        },
        moveInstructionUp: (state, action: PayloadAction<number>) => {
            const moveIndex = action.payload;
            if (moveIndex === 0) {
                return;
            }
            const temp = state.instructions[moveIndex];
            state.instructions[moveIndex] = state.instructions[moveIndex - 1];
            state.instructions[moveIndex - 1] = temp;
        },
        moveInstructionDown: (state, action: PayloadAction<number>) => {
            const moveIndex = action.payload;
            if (moveIndex === state.instructions.length - 1) {
                return;
            }
            const temp = state.instructions[moveIndex];
            state.instructions[moveIndex] = state.instructions[moveIndex + 1];
            state.instructions[moveIndex + 1] = temp;
        },
        setIngredient: (
            state,
            action: PayloadAction<{ index: number; field: string; value: any }>
        ) => {
            const { index, field, value } = action.payload;
            state.ingredients[index][field] = value;
        },
        addIngredient: (state) => {
            state.ingredients.push(newIngredient());
        },
        addSubtitle: (state) => {
            state.ingredients.push(newSubtitle());
        },
        removeIngredient: (state, action: PayloadAction<number>) => {
            state.ingredients.splice(action.payload, 1);
        },
        moveIngredientUp: (state, action: PayloadAction<number>) => {
            const moveIndex = action.payload;
            if (moveIndex === 0) {
                return;
            }
            const temp = state.ingredients[moveIndex];
            state.ingredients[moveIndex] = state.ingredients[moveIndex - 1];
            state.ingredients[moveIndex - 1] = temp;
        },
        moveIngredientDown: (state, action: PayloadAction<number>) => {
            const moveIndex = action.payload;
            if (moveIndex === state.ingredients.length - 1) {
                return;
            }
            const temp = state.ingredients[moveIndex];
            state.ingredients[moveIndex] = state.ingredients[moveIndex + 1];
            state.ingredients[moveIndex + 1] = temp;
        },
    },
});

export const editRecipeReducer = editRecipeSlice.reducer;
export const editRecipeActions = editRecipeSlice.actions;
