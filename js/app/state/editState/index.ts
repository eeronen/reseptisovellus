import { combineReducers } from "redux";
import { editRecipeReducer } from "./currentRecipe";
import { selectedFieldReducer } from "./selectedField";

export const editStateReducer = combineReducers({
    recipe: editRecipeReducer,
    selectedField: selectedFieldReducer,
});
