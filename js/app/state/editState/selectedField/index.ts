import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export const enum FIELD_TYPES {
    INGREDIENTS = "INGREDIENTS",
    INSTRUCTIONS = "INSTRUCTIONS",
}

const selectedFieldSlice = createSlice({
    name: "selectedEditField",
    initialState: {
        index: undefined as number | undefined,
        field: undefined as FIELD_TYPES | undefined,
    },
    reducers: {
        setEditableField: (state, action: PayloadAction<{ index: number; field: FIELD_TYPES }>) => {
            state.field = action.payload.field;
            state.index = action.payload.index;
        },
        resetEditableField: (state) => {
            state.field = undefined;
            state.index = undefined;
        },
    },
});

export const selectedFieldReducer = selectedFieldSlice.reducer;
export const selectedFieldActions = selectedFieldSlice.actions;
