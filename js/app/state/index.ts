import { TypedUseSelectorHook, useSelector } from "react-redux";
import { StoreState as StateType, applicationReducer } from "./reducer";
import * as stateSelectors from "./selectors";
import { configureStore } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
export const selectors = stateSelectors;

export const store = configureStore({
    reducer: applicationReducer,
});

export type StoreState = StateType;
export type GetState = typeof store.getState;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = useDispatch<typeof store.dispatch>;
export const useAppSelector: TypedUseSelectorHook<StoreState> = useSelector;
