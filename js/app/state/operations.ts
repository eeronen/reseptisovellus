import { AppDispatch, GetState, selectors } from ".";
import { NEW_RECIPE } from "../recipe";
import { editRecipeActions } from "./editState/currentRecipe";
import { getRecipeByName } from "./selectors";
import { viewStateAction } from "./viewState";

export const setPortionsMultiplierWithPortionsNumber =
    (recipeName: string, newPortions: number) => (dispatch: AppDispatch, getState: GetState) => {
        if (newPortions > 0) {
            const currentRecipe = selectors.getRecipeByName(getState(), recipeName);
            if (currentRecipe === undefined) {
                return;
            }
            const recipePortions = currentRecipe.portions || 1;
            const portionMultiplier = newPortions !== 0 ? newPortions / recipePortions : 1;
            if (portionMultiplier !== selectors.getPortionsMultiplier(getState())) {
                dispatch(viewStateAction.setPortionsMultiplier(portionMultiplier));
            }
        }
    };

export const resetEditableRecipe =
    (recipeName: string) => (dispatch: AppDispatch, getState: GetState) => {
        if (recipeName === "uusi") {
            dispatch(editRecipeActions.initializeEditableRecipe(NEW_RECIPE));
        } else {
            dispatch(
                editRecipeActions.initializeEditableRecipe(
                    JSON.parse(JSON.stringify(getRecipeByName(getState(), recipeName)))
                )
            );
        }
    };
