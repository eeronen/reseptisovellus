import { Recipe } from "../../recipe";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState = null as Recipe[] | null;

const recipesSlice = createSlice({
    name: "recipes",
    initialState,
    reducers: {
        setAllRecipes: (_, action: PayloadAction<Recipe[]>) => {
            return action.payload;
        },
    },
});

export const recipesReducer = recipesSlice.reducer;
export const recipesActions = recipesSlice.actions;
