import { combineReducers } from "redux";
import { editStateReducer } from "./editState";
import { viewStateReducer } from "./viewState";
import { recipesReducer } from "./recipes";
import { appReducer } from "./appState";

export const applicationReducer = combineReducers({
    editState: editStateReducer,
    viewState: viewStateReducer,
    recipes: recipesReducer,
    appState: appReducer,
});

export type StoreState = ReturnType<typeof applicationReducer>;
