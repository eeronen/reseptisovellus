import type { UnitName } from "../../utils/unitChanger";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState = {
    portionMultiplier: 1,
    isInEditMode: false,
    editableIndex: undefined as number | undefined,
    appliedFilters: [] as string[],
    selectedUnits: [] as (UnitName | null)[],
};

const viewStateSlice = createSlice({
    initialState,
    name: "viewState",
    reducers: {
        setPortionsMultiplier: (state, action: PayloadAction<number>) => {
            state.portionMultiplier = action.payload;
        },
        setEditableIndex: (state, action: PayloadAction<number>) => {
            state.editableIndex = action.payload;
            state.isInEditMode = true;
        },
        resetEditMode: (state) => {
            state.editableIndex = undefined;
            state.isInEditMode = false;
        },
        addFilter: (state, action: PayloadAction<string>) => {
            state.appliedFilters.push(action.payload);
        },
        resetFilters: (state) => {
            state.appliedFilters = [];
        },
        removeFilter: (state, action: PayloadAction<string>) => {
            const removeIndex = state.appliedFilters.findIndex(
                (filter) => filter === action.payload
            );
            state.appliedFilters.splice(removeIndex, 1);
        },
        setUnit: (state, action: PayloadAction<{ index: number; unit: string }>) => {
            const { index, unit } = action.payload;
            state.selectedUnits[index] = unit;
        },
        resetUnits: (state) => {
            for (let i = 0; i < state.selectedUnits.length; i++) {
                state.selectedUnits[i] = null;
            }
        },
    },
});

export const viewStateAction = viewStateSlice.actions;
export const viewStateReducer = viewStateSlice.reducer;
