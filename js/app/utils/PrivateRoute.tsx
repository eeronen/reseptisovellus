import * as React from "react";
import { login, checkAuthentication } from "../services/authService";
import { selectors, useAppDispatch, useAppSelector } from "../state";
import { appActions } from "../state/appState";

export const PrivateRoute: React.FC<{ element: React.ReactElement }> = ({ element }) => {
    const dispatch = useAppDispatch();

    const updateLoginStatus = React.useCallback(async () => {
        try {
            const isValid = await checkAuthentication();
            dispatch(appActions.setLoggedIn(isValid));
        } catch (error) {
            dispatch(appActions.setLoggedIn(false));
        }
    }, [dispatch]);

    const [isLoaded, setIsLoaded] = React.useState(false);
    React.useEffect(() => {
        updateLoginStatus().finally(() => {
            setIsLoaded(true);
        });
    }, []);

    const isLoggedIn = useAppSelector((state) => selectors.isLoggedIn(state));

    if (isLoaded) {
        if (isLoggedIn) {
            return element;
        } else {
            return <LoginPage />;
        }
    } else {
        return null;
    }
};

const LoginPage: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const [isFirstTry, setIsFirstTry] = React.useState(true);
    const [password, setPassword] = React.useState("");

    const handleLogin = React.useCallback(async () => {
        try {
            await login(password);
            dispatch(appActions.setLoggedIn(true));
        } catch (error) {
            dispatch(appActions.setLoggedIn(false));
            setIsFirstTry(false);
        }
    }, [dispatch, password]);

    const onInputKeyDown = React.useCallback(
        (event: React.KeyboardEvent<HTMLInputElement>) => {
            if (event.key === "Enter") {
                handleLogin();
            }
        },
        [handleLogin]
    );

    const onInputChange = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
    }, []);

    return (
        <div className="content">
            <div className="login-header">Anna salasana reseptien muokkaukseen</div>
            <div className="login-page__content">
                <input
                    type="password"
                    className={isFirstTry ? "password-input" : "password-input incorrect-password"}
                    onKeyDown={onInputKeyDown}
                    value={password}
                    onChange={onInputChange}
                />
                {isFirstTry ? null : (
                    <div className="login-incorrect-password-text">Virheellinen salasana</div>
                )}
                <button className="submitbutton login" onClick={handleLogin}>
                    Kirjaudu
                </button>
            </div>
        </div>
    );
};
