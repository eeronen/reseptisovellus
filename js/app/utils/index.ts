export * from "./inputUtils";

export function classNames(classes: { [className: string]: boolean }) {
    return Object.keys(classes)
        .filter(className => classes[className])
        .join(" ");
}
