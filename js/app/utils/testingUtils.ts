import * as React from "react";
import { spy, replace } from "sinon";

export const fakeReactUseState = (initialState: any) => {
    const setState = spy();
    //@ts-ignore
    replace(React, "useState", () => [initialState, setState]);
    return setState;
};
