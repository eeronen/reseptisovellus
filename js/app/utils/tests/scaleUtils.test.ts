import "mocha";
import { expect } from "chai";

import { makeInstructionNumberScaler } from "../scaleUtils";

describe("unitChanger", () => {
    const scaleUnits = makeInstructionNumberScaler(2);
    it("Should scale marked numbers", () => {
        expect(scaleUnits("Add $1 cups of sugar")).to.equal("Add 2 cups of sugar");
    });

    it("Should replace multiple numbers", () => {
        expect(scaleUnits("Add $1 to $2 cups of sugar")).to.equal("Add 2 to 4 cups of sugar");
    });

    it("Should replace decimals with dot notation", () => {
        expect(scaleUnits("Add $1.5 cups of sugar")).to.equal("Add 3 cups of sugar");
    });

    it("Should replace decimals with comma notation", () => {
        expect(scaleUnits("Add $1,5 cups of sugar")).to.equal("Add 3 cups of sugar");
    });

    it("Should round to one decimal", () => {
        const scaleUnevenNumber = makeInstructionNumberScaler(0.333333);
        expect(scaleUnevenNumber("Add $3 cups of sugar")).to.equal("Add 1 cups of sugar");
        expect(scaleUnevenNumber("Add $2 cups of sugar")).to.equal("Add 0.7 cups of sugar");
    });
});
