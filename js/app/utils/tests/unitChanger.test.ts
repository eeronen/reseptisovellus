import "mocha";
import { expect } from "chai";

import {
    getAsBaseUnit,
    getAsOtherUnit,
    getBaseUnitOf,
    getPossibleUnits,
    MeasureType,
} from "../unitChanger";

describe("unitChanger", () => {
    it("Should get base units", () => {
        expect(getBaseUnitOf("l")).to.deep.equal({
            name: "ml",
            measure: MeasureType.VOLUME,
            isBase: true,
            multiplierToBase: 1,
        });
        expect(getBaseUnitOf("tl")).to.deep.equal({
            name: "ml",
            measure: MeasureType.VOLUME,
            isBase: true,
            multiplierToBase: 1,
        });
    });

    it("Should convert to base units", () => {
        expect(getAsBaseUnit("tl", 2)).to.deep.equal({ unit: "ml", quantity: 10 });
        expect(getAsBaseUnit("l", 1)).to.deep.equal({ unit: "ml", quantity: 1000 });
    });

    it("Should convert to same unit", () => {
        expect(getAsOtherUnit("rkl", 2, "rkl")).to.equal(2);
        expect(getAsOtherUnit("foobar", 42, "foobar")).to.equal(42);
    });

    it("Should convert to another unit", () => {
        expect(getAsOtherUnit("rkl", 2, "dl")).to.equal(0.3);
        expect(getAsOtherUnit("rkl", 1, "tl")).to.equal(3);
    });

    it("Should return -1 for unknown units", () => {
        expect(getAsOtherUnit("rkl", 1, "foobar")).to.equal(-1);
    });

    it("Should return self for unknown base unit", () => {
        expect(getAsBaseUnit("foobar", 42)).to.deep.equal({ unit: "foobar", quantity: 42 });
    });

    it("Should get all units with same type", () => {
        expect(getPossibleUnits("ml")).to.deep.equal(["ml", "tl", "rkl", "dl", "kuppia", "l"]);
    });
});
