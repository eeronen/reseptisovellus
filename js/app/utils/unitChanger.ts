export type UnitName = string;

type Unit = {
    name: string;
    measure: MeasureType;
    isBase: boolean;
    multiplierToBase: number;
};

export const enum MeasureType {
    VOLUME,
    WEIGHT,
    UNDEFINED,
}

const UNITS: Unit[] = [
    {
        name: "ml",
        measure: MeasureType.VOLUME,
        isBase: true,
        multiplierToBase: 1,
    },
    {
        name: "tl",
        measure: MeasureType.VOLUME,
        isBase: false,
        multiplierToBase: 5,
    },
    {
        name: "rkl",
        measure: MeasureType.VOLUME,
        isBase: false,
        multiplierToBase: 15,
    },
    {
        name: "dl",
        measure: MeasureType.VOLUME,
        isBase: false,
        multiplierToBase: 100,
    },
    {
        name: "kuppia",
        measure: MeasureType.VOLUME,
        isBase: false,
        multiplierToBase: 284.131,
    },
    {
        name: "l",
        measure: MeasureType.VOLUME,
        isBase: false,
        multiplierToBase: 1000,
    },
    {
        name: "g",
        measure: MeasureType.WEIGHT,
        isBase: true,
        multiplierToBase: 1,
    },
    {
        name: "kg",
        measure: MeasureType.WEIGHT,
        isBase: false,
        multiplierToBase: 1000,
    },
];

export function getAsOtherUnit(
    startUnit: string | undefined,
    quantity: number | undefined,
    endUnit: string | undefined
): number {
    if (quantity === undefined) {
        return 0;
    } else if (startUnit === endUnit || startUnit === undefined || endUnit === undefined) {
        return quantity;
    } else {
        const base = getAsBaseUnit(startUnit, quantity);
        return getBaseAsOtherUnit(base.quantity, endUnit);
    }
}

export function getAsBaseUnit(
    unitName: string,
    quantity: number
): { unit: string; quantity: number } {
    const startUnit = getUnitByName(unitName);
    if (startUnit) {
        const baseUnit = getBaseUnitOf(startUnit.name);
        if (baseUnit !== undefined) {
            return { unit: baseUnit.name, quantity: quantity * startUnit.multiplierToBase };
        }
    }
    return { unit: unitName, quantity: quantity };
}

function getBaseAsOtherUnit(quantity: number, unitName: string): number {
    const unit = getUnitByName(unitName);
    if (unit) {
        return quantity / unit.multiplierToBase;
    } else {
        return -1;
    }
}

export function getBaseUnitOf(unitName: string): Unit | undefined {
    const startUnit = getUnitByName(unitName);
    if (startUnit === undefined) {
        return {
            name: "undefined",
            isBase: false,
            measure: MeasureType.UNDEFINED,
            multiplierToBase: 1,
        };
    }
    return getBaseUnits().find((unit) => unit.measure === startUnit.measure);
}

function getUnitByName(unitName: string): Unit | undefined {
    return UNITS.find((unit) => unit.name === unitName);
}

function getBaseUnits(): Unit[] {
    return UNITS.filter((unit) => unit.isBase);
}

export function getPossibleUnits(unitName?: string) {
    if (unitName === undefined) {
        return [];
    }
    const currentUnit = getUnitByName(unitName);
    if (currentUnit) {
        return UNITS.filter((unit) => unit.measure === currentUnit.measure).map(
            (unit) => unit.name
        );
    } else {
        return [];
    }
}
