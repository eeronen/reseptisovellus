import { versions, Version } from "../versions";

const VERSION_KEY = "last_seen_version";

export function getLatestVersionInfo(): Version {
    return getVersionInfo(getLatestVersionNumber());
}

export function markLatestVersionAsSeen() {
    markVersionAsSeen(getLatestVersionNumber());
}

export function getVersionNumbers(): string[] {
    return versions.map(version => version.number);
}

export function getVersionInfo(numberOrName: string): Version {
    return versions.find(
        version => version.number === numberOrName || version.name === numberOrName
    );
}

export function getNewFeaturesSince(versionNumber: string): string[] {
    const sinceIndex = versions.findIndex(version => version.number === versionNumber);
    return versions
        .slice(sinceIndex + 1)
        .reduce((features, version) => features.concat(version.features), []);
}

export function isLatestVersion(number: string): boolean {
    return getLatestVersionNumber() === number;
}

export function getLatestVersionNumber(): string {
    return versions[versions.length - 1].number;
}

export function markVersionAsSeen(version: string) {
    window.localStorage.setItem(VERSION_KEY, version);
}

export function getLastSeenVersion() {
    return window.localStorage.getItem(VERSION_KEY);
}

export function isLatestVersionSeen(): boolean {
    return isLatestVersion(getLastSeenVersion());
}
