export interface Version {
    releaseDate: string;
    number: string;
    name?: string;
    features: string[];
}

export const versions: Version[] = [
    {
        releaseDate: "25.8.2018",
        number: "1.0.1",
        name: "Aioli",
        features: ["Versionumerointi otettu käyttöön", "Mahdollisuus hakea reseptejä"],
    },
    {
        releaseDate: "21.3.2019",
        number: "1.1.0",
        name: "Bechamel",
        features: ["Performanssiparannuksia"],
    },
    {
        releaseDate: "14.10.2019",
        number: "1.2.0",
        name: "",
        features: ["Reseptin voi tallentaa välilehteen. Paina kirjanmerkin kuvaa reseptissä."],
    },
];
