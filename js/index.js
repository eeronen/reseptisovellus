import * as React from "react";
import { createRoot } from "react-dom/client";
import { App } from "./app/App";
import "./app/styles/styles.scss";

const reactRoot = createRoot(document.getElementById("app"));

reactRoot.render(<App />);
