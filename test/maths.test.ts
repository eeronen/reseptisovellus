import { expect } from "chai";
import "mocha";
import { RoundTo2Decimals, RoundToOneDecimal, RoundToQuarters } from "../js/app/utils/maths";

describe("maths", () => {
    it("Should round to 2 decimals", () => {
        expect(RoundTo2Decimals(1)).to.equal(1);
        expect(RoundTo2Decimals(1.1234567)).to.equal(1.12);
        expect(RoundTo2Decimals(1.55555555)).to.equal(1.56);
    });

    it("Should round to 1 decimal", () => {
        expect(RoundToOneDecimal(1)).to.equal(1);
        expect(RoundToOneDecimal(1.1234567)).to.equal(1.1);
        expect(RoundToOneDecimal(1.55555555)).to.equal(1.6);
    });

    it("Should round to quarters", () => {
        expect(RoundToQuarters(1)).to.equal(1);
        expect(RoundToQuarters(1.125)).to.equal(1.25);
        expect(RoundToQuarters(1.124)).to.equal(1);
    });
});
