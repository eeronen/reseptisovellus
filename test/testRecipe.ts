import { Recipe } from "../js/app/recipe";

export const TEST_RECIPE: Recipe[] = [
    {
        name: "recipe1",
        description: "",
        category: "muut",
        cookingTime: 1,
        portions: 1,
        ingredients: [{ name: "foo" }],
        instructions: ["instruction1"],
        picture: null,
    },
    {
        name: "recipe2",
        description: "",
        category: "muut",
        cookingTime: 1,
        portions: 1,
        ingredients: [{ name: "foo" }],
        instructions: ["instruction1"],
        picture: null,
    },
    {
        name: "recipe3",
        description: "",
        category: "muut",
        cookingTime: 1,
        portions: 1,
        ingredients: [{ name: "bar" }],
        instructions: ["instruction1"],
        picture: null,
    },
];
