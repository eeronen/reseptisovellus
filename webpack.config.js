const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: "development",
    devtool: "source-map",
    entry: path.join(__dirname, "js", "index.js"),
    output: {
        filename: "[contenthash].bundle.js",
        path: path.join(__dirname, "build"),
    },
    module: {
        rules: [
            {
                test: /\.(tsx?|jsx?)$/,
                loader: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    "css-loader",
                    "sass-loader",
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                exclude: /\.inline\.svg/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: path.join("assets", "[contenthash].[ext]"),
                        },
                    },
                ],
            },
            {
                test: /\.inline\.svg/,
                loader: "svg-react-loader",
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({ filename: "[contenthash].styles.css" }),
        new CopyWebpackPlugin({
            patterns: [
                path.join("js", "manifest.json"),
                "favicon.png",
                {
                    from: path.join("js", "assets", "PWA-icon.png"),
                    to: "assets",
                },
                {
                    from: path.join("js", "api"),
                    to: "api",
                },
            ],
        }),
    ],
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx", ".scss"],
    },
};
