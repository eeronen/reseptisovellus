const path = require("path");
const { merge } = require("webpack-merge");
const { DefinePlugin } = require("webpack");
const common = require("./webpack.config.js");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = merge(common, {
    devtool: "source-map",
    output: {
        filename: "main.bundle.js",
        path: path.resolve(__dirname, "build"),
    },
    plugins: [
        new MiniCssExtractPlugin({ filename: "styles.css" }),
        new DefinePlugin({
            "process.env.NODE_ENV": JSON.stringify("development"),
        }),
        new HTMLWebpackPlugin({
            template: path.join(__dirname, "js", "index.html"),
            filename: "index.html",
            inject: "body",
        }),
    ],
});
