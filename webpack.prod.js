const path = require("path");
const { merge } = require("webpack-merge");
const { DefinePlugin } = require("webpack");
const common = require("./webpack.config.js");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HTMLWebpackPlugin = require("html-webpack-plugin");

module.exports = merge(common, {
    mode: "production",
    plugins: [
        new DefinePlugin({
            "process.env.NODE_ENV": JSON.stringify("production"),
        }),
        new CleanWebpackPlugin(),
        new HTMLWebpackPlugin({
            template: path.join(__dirname, "js", "index.html"),
            filename: "index.html",
            inject: "body",
        }),
    ],
});
